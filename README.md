
## Descrição

Este projeto foi desenvolvido utilizando o [Lumen 8.3.1](https://lumen.laravel.com/docs/8.x) como framework backend.

# Instalação

Instalar as dependências do projeto
```bash
$ composer install 
$ composer dump-autoload
$ php artisan key:generate
```
## Configuração

Crie um arquivo .env e copie o conteúdo da .env.example para dentro dele.

Feito isso, crie um banco de dados mysql com o nome de ot_blockchain

 
## Formas de rodar a aplicação
### Docker
Para rodar a aplicação com o docker basta ter ele e o docker-compose instalados em sua máquina e rodar o comando:
```bash
$ docker-compose up --build
```

### Servidor PHP 
Será necessário algum serviço para que rode o mysql em sua maquina e para rodar o projeto através do php rode o seguinte comando:

```bash
$ php -S localhost:8000 -t public
```

## Testes

```bash
$ vendor\bin\phpunit
```

## Swagger

```bash
$ php artisan swagger-lume:generate
```

## Passport
necessário rodar migration. 

```bash
$ php artisan passport:install --force
```

Escolha o primeiro provider

## Rodar os jobs 

```bash
$ php artisan queue:work --queue transactions
ou
$ php artisan queue:listen --queue transactions
```