<?php

namespace Database\Factories;

use App\Models\Frequencia;
use Illuminate\Database\Eloquent\Factories\Factory;

class FrequenciaFactory extends Factory
{
    protected $model = Frequencia::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name,
            "label" => $this->faker->text(10),
            "created_by" => "any_one",
        ];
    }
}
