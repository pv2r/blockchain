<?php

namespace Database\Factories;

use App\Models\Algoritmo;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlgoritmoFactory extends Factory
{
    protected $model = Algoritmo::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name,
            "label" => $this->faker->text(10),
            "created_by" => "any_one",
        ];
    }
}
