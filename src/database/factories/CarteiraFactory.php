<?php

namespace Database\Factories;

use App\Models\Algoritmo;
use App\Models\Carteira;
use App\Models\Categoria;
use App\Models\Frequencia;
use App\Models\Perfil;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarteiraFactory extends Factory
{
    protected $model = Carteira::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "description" => $this->faker->text(50),
            "account_uid" => 1,
            "algorithm_uid" => Algoritmo::all()->random()->algorithm_uid,
            "profile_uid" => Perfil::all()->random()->profile_uid,
            "category_uid" => Categoria::all()->random()->category_uid,
            "frequency_uid" => Frequencia::all()->random()->frequency_uid,
            "address" => $this->faker->uuid,
            "public_key" => $this->faker->uuid,
        ];
    }
}
