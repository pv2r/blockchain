<?php

use App\Models\TransacaoTipo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSignedStatus extends Migration
{
    private $status = 
        [
            'name' => 'signed',
            'label' => 'Signed',
            'created_by' => 'Fintools'
        ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $existe = TransacaoTipo::where('name', $this->status['name'])->count() > 0;
        if(!$existe){
            TransacaoTipo::create($this->status);
        }
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TransacaoTipo::where('name', $this->status['name'])->delete();
    }
}
