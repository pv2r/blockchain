<?php

use App\Models\Algoritmo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlgoritmos extends Migration
{

    private $algotitmos = [
        [
            'name' => 'ECDSA',
            'label' => 'Elliptic Curve Digital Signature Algorithm - ECDSA',
            'created_by' => 'Fintools'
        ],
        [
            'name' => 'RSA',
            'label' => 'RSA',
            'created_by' => 'Fintools'
        ],
        [
            'name' => 'AES',
            'label' => 'AES',
            'created_by' => 'Fintools'
        ],
    ];
    public function up()
    {
        foreach($this->algotitmos as $algotitmo){
            $existe = Algoritmo::where('name', $algotitmo['name'])->count() > 0;
            if(!$existe){
                Algoritmo::create($algotitmo);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->algotitmos as $algotitmo){
            Algoritmo::where('name', $algotitmo['name'])->delete();
        }
    }
}
