<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarteirasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->uuid('wallet_uid')->primary();
//            $table->foreignUuid('account_uid')
//                ->nullable()
//                ->references('account_uid')
//                ->on('usuarios');

            $table->string('account_uid', 36)->nullable();
            $table->string('description', 180);
            $table->foreignUuid('profile_uid')
                ->references('profile_uid')
                ->on('profiles');
            $table->foreignUuid('category_uid')
                ->references('category_uid')
                ->on('categories');
            $table->foreignUuid('frequency_uid')
                ->references('frequency_uid')
                ->on('frequencies');
            $table->string('address', 42);
            $table->string('public_key', 132);
            $table->string('created_by', 80)->nullable()->default(null);
            $table->timestamp('created_at', 0)->nullable();
            $table->string('updated_by', 80)->nullable()->default(null);
            $table->timestamp('updated_at', 0)->nullable();
            $table->string('deleted_by', 80)->nullable()->default(null);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
