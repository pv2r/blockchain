<?php

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequencies', function (Blueprint $table) {
            $table->uuid('frequency_uid')->primary();
            $table->string('name', 80)->index();
            $table->string('label', 120);
            $table->string('created_by', 80)->nullable()->default(null);
            $table->timestamp('created_at', 0)->nullable();
            $table->string('updated_by', 80)->nullable()->default(null);
            $table->timestamp('updated_at', 0)->nullable();
            $table->string('deleted_by', 80)->nullable()->default(null);
            $table->softDeletes();
        });

         //dados default para o sistema
         $dados = [
            [
                'frequency_uid' => Uuid::uuid4()->toString(),
                'name' => 'low',
                'label' => 'Low',
                'created_by' => 'fintools',
            ],
            [
                'frequency_uid' => Uuid::uuid4()->toString(),
                'name' => 'medium',
                'label' => 'Medium',
                'created_by' => 'fintools',
            ],
            [
                'frequency_uid' => Uuid::uuid4()->toString(),
                'name' => 'high',
                'label' => 'High',
                'created_by' => 'fintools',
            ]
        ];
        DB::table("frequencies")->insert($dados);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequencies');
    }
}
