<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTransactionGasLimitGasPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(env('APP_ENV') == 'testing') {
            return;
        }
        Schema::table('transactions', function(Blueprint $table) {
            $table->renameColumn('gasPrice', 'gas_price');
            $table->renameColumn('gasLimit', 'gas_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function(Blueprint $table) {
            $table->renameColumn('gas_price', 'gasPrice');
            $table->renameColumn('gas_limit', 'gasLimit');
        });
    }
}
