<?php

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->uuid('profile_uid')->primary();
            $table->string('name', 80)->index();
            $table->string('label', 120);
            $table->string('created_by', 80)->nullable()->default(null);
            $table->timestamp('created_at', 0)->nullable();
            $table->string('updated_by', 80)->nullable()->default(null);
            $table->timestamp('updated_at', 0)->nullable();
            $table->string('deleted_by', 80)->nullable()->default(null);
            $table->softDeletes();
        });

        //dados default para o sistema
        $dados = [
            [
                'profile_uid' => Uuid::uuid4()->toString(),
                'name' => 'people',
                'label' => 'People',
                'created_by' => 'fintools',
            ],
            [
                'profile_uid' => Uuid::uuid4()->toString(),
                'name' => 'application',
                'label' => 'Application',
                'created_by' => 'fintools',
            ]
        ];
        DB::table("profiles")->insert($dados);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
