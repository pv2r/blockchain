<?php

use App\Models\Rede;
use App\Models\Algoritmo;
use App\Services\RedeService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRopstenNetwork extends Migration
{
    private $rede = [
        'name' => 'ropsten',
        'label' => 'Ropsten',
        'coin' => 'ETH',
        'provider' => 'https://eth-ropsten.alchemyapi.io/v2/JhWPDt7l-c_EVjlFD34IwgFilre2hxo2',
        'algorithm_uid' => '',
        'created_by' => 'Fintools',
        'updated_by' => 'Fintools',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(env('APP_ENV') == 'testing') {
            return;
        }
        try{
            $redeService = new RedeService();
            $algoritmo = Algoritmo::where('name', 'AES')->first();
            $this->rede['algorithm_uid'] = $algoritmo->algorithm_uid;
            $rede = Rede::where('name', $this->rede['name'])->first();
            if(!$rede){
                $nova_rede = Rede::create($this->rede);
                $redeService->store($nova_rede);
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try{
            $redeService = new RedeService();
            $rede = Rede::where('name', $this->rede['name'])->first();
            if($rede){
                $redeService->delete($rede->network_uid);
                $rede->forceDelete();
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }

    }
}
