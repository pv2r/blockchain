<?php

use App\Models\TransacaoTipo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class AddTransactionStatus extends Migration
{
    private $transaction_status = [
        [
            'name' => 'processing',
            'label' => 'Processing',
            'created_by' => 'Fintools'
        ],
        [
            'name' => 'done',
            'label' => 'Done',
            'created_by' => 'Fintools'
        ],
        [
            'name' => 'error',
            'label' => 'Error',
            'created_by' => 'Fintools'
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->transaction_status as $status){
            $existe = TransacaoTipo::where('name', $status['name'])->count() > 0;
            if(!$existe){
                TransacaoTipo::create($status);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->transaction_status as $status){
            TransacaoTipo::where('name', $status['name'])->delete();
        }
    }

}
