<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTransactionsAddTransactionRaw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(env('APP_ENV') == 'testing') {
            return;
        }
        Schema::table('transactions', function(Blueprint $table) {
            $table->boolean('raw_transaction')->default(false)->after('response_hash');
            $table->string('gas_price')->nullable()->change();
            $table->string('gas_limit')->nullable()->change();
            $table->string('nonce')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function(Blueprint $table) {
            $table->dropColumn('raw_transaction');
            $table->string('gas_price')->change();
            $table->string('gas_limit')->change();
            $table->string('nonce')->change();
        });
    }
}
