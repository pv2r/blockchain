<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('transaction_uid')->primary();
            $table->string('network_uid', 36);
            $table->string('from_address', 132)
                ->references('wallet_uid')
                ->on('wallets');
            $table->string('to_address', 132)
                ->references('wallet_uid')
                ->on('wallets');
            $table->string('nonce');
            $table->string('gasPrice');
            $table->string('gasLimit');
            $table->string('value');
            $table->string('data');
            $table->string('status');
            $table->string('status_description', 180)->nullable()->default(null);
            $table->string('response_hash')->nullable()->default(null);
            $table->string('created_by', 80)->nullable()->default(null);
            $table->timestamp('created_at', 0)->nullable();
            $table->string('updated_by', 80)->nullable()->default(null);
            $table->timestamp('updated_at', 0)->nullable();
            $table->string('deleted_by', 80)->nullable()->default(null);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
