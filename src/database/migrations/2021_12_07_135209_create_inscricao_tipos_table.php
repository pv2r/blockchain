<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Ramsey\Uuid\Uuid;

class CreateInscricaoTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_types', function (Blueprint $table) {
            $table->uuid('document_type_uid')->primary();
            $table->string('name', 80)->index();
            $table->string('label', 120);
            $table->string('created_by', 80)->nullable()->default(null);
            $table->timestamp('created_at', 0)->nullable();
            $table->string('updated_by', 80)->nullable()->default(null);
            $table->timestamp('updated_at', 0)->nullable();
            $table->string('deleted_by', 80)->nullable()->default(null);
            $table->softDeletes();
        });

        //dados default para o sistema
        $dados = [
            [
                'document_type_uid' => Uuid::uuid4()->toString(),
                'name' => 'cnpj',
                'label' => 'CNPJ',
                'created_by' => 'fintools',
            ],
            [
                'document_type_uid' => Uuid::uuid4()->toString(),
                'name' => 'cpf',
                'label' => 'CPF',
                'created_by' => 'fintools',
            ]
        ];
        DB::table("document_types")->insert($dados);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_types');
    }
}
