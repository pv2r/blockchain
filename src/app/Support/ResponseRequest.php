<?php

namespace App\Support;

use Illuminate\Http\JsonResponse;

class ResponseRequest
{
    /**
     * @param $data
     * @param bool $status
     * @param string $message
     * @param int $status_code
     * @return JsonResponse
     */
    public function execute($data, bool $status = true, string $message = '', int $status_code = 200) : JsonResponse
    {
        return response()->json([
            'success' => $status,
            'message' => $message,
            'data' => $data
        ], $status_code);
    }
}
