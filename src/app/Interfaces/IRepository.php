<?php


namespace App\Interfaces;


use Illuminate\Http\Request;

interface IRepository
{
    public function store(array $dados);
    public function update(string $uid, array $dados);
    public function destroy(string $uid, string $deleted_by);
    public function findById(string $uid);
    public function filter(array $search, array $searchWith);
    public function advancedFilter(Request $request);
    public function getAll();
    public function getAllPaginated(int $perPage);
}
