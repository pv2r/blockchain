<?php

namespace App\Jobs;

use Throwable;
use App\Models\Transacao;
use Illuminate\Bus\Queueable;
use App\Services\TransacaoService;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Repositories\TransacaoRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Facades\Log;

class SignTransactionJob extends Job
{

    protected $transacao;
    /**
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     *
     */
    public function __construct(Transacao $transacao)
    {
        $this->onQueue('blockchain_'.config('app.env').'_transactions');
        $this->transacao = $transacao;

    }

    /**
     * @throws \Exception
     */
    public function handle(TransacaoService $service)
    {
        $response = $service->sign($this->transacao);
        $repository = new TransacaoRepository($this->transacao);
        $repository->aprovarTransacao($this->transacao->transaction_uid, $response->data->signed_transaction);
    }
    /**
     * Handle a job failure.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        $repository = new TransacaoRepository($this->transacao);
        return $repository->erroTransacao($this->transacao->transaction_uid, $exception->getMessage());
    }
    /**
     * Calculate the number of seconds to wait before retrying the job.
     *
     * @return array
     */
    public function backoff()
    {
        return [1, 5, 10];
    }
}
