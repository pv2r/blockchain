<?php

namespace App\Jobs;

use App\Messages\Messages;
use App\Models\Transacao;
use App\Repositories\TransacaoRepository;
use App\Services\TransacaoService;
use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class SendTransactionJob extends Job
{
    protected $transacao;
    /**
     * @var int
     */
    public $tries = 3;



    /**
     * Create a new job instance.
     *
     *
     */
    public function __construct(Transacao $transacao)
    {
        $this->onQueue('blockchain_'.config('app.env').'_transactions');
        $this->transacao = $transacao;
    }

    /**
     * @param TransacaoService $service
     * @param TransacaoRepository $transacaoRepository
     * @return void
     * @throws Exception
     */
    public function handle(TransacaoService $service)
    {
        $response = $service->create($this->transacao);
        $transacaoRepository = new TransacaoRepository($this->transacao);
        $transacaoRepository->aprovarTransacao($this->transacao->transaction_uid, $response->hash);
    }
    /**
     * Handle a job failure.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        $repository = new TransacaoRepository(Transacao::class);
        return $repository->erroTransacao($this->transacao->transaction_uid, $exception->getMessage());
    }
    /**
     * Calculate the number of seconds to wait before retrying the job.
     *
     * @return array
     */
    public function backoff()
    {
        return [1, 5, 10];
    }
}
