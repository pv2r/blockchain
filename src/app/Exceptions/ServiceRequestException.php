<?php

namespace App\Exceptions;
use App\Messages\Messages;
use Exception;
use Illuminate\Http\JsonResponse;

class ServiceRequestException extends Exception
{
    use Messages;
    public function render(): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $this->message != ""? $this->message : $this->message('erro.interno.realizar.busca'),
            'data' => []
        ], 500);
    }
}
