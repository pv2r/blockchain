<?php

namespace App\Repositories;


use App\Jobs\SendTransactionJob;
use App\Messages\Messages;
use App\Models\Carteira;
use App\Models\Rede;
use App\Models\TransacaoTipo;
use Illuminate\Support\Facades\Log;

class TransacaoRepository extends BaseRepository
{
    protected $model;

    public function __construct($model)
    {
        parent::__construct(__CLASS__);
        $this->model = $model;
        return $this;
    }

    public function store(Array $transacao)
    {
        try {
            $status = TransacaoTipo::where('name', 'processing')->first();
            $status_uid = $status->transaction_status_uid;
            $novaTransacao = [
                'network_uid' => $transacao['network_uid'],
                'status' => $status_uid
            ];
            foreach($transacao['data'] as $key => $value) {
                $novaTransacao[$key] = $value;
            }

            return parent::store($novaTransacao);
        }catch (\Exception $e) {
            Log::error($e);
            throw new \Exception(Messages::message('falha.interna.criar.dados'), 400);
        }

    }

    /**
     * @throws \Exception
     */
    public function signTransaction(Array $transacao)
    {
        $from = Carteira::where('address', $transacao["from_address"])->first();
        $to = Carteira::where('address', $transacao["to_address"])->first();
        $network = Rede::where('network_uid', $transacao["network_uid"])->first();
        if($to->algorithm_uid != $from->algorithm_uid || $to->algorithm_uid != $network->algorithm_uid){
            throw new \Exception("Transaction between different algorithms not allowed!", 400);
        }
        $status = TransacaoTipo::where('name', 'processing')->first();

        $status_uid = $status->transaction_status_uid;
        return parent::store([
            'status' => $status_uid,
            'raw_transaction' => true,
            ...$transacao
        ]);
    }

    public function aprovarTransacao($transacao_uid, $hashResponse): void
    {
        $transacao = $this->findById($transacao_uid);
        if(!$transacao){
            return;
        }
        $aprovado = TransacaoTipo::where('name', 'done')->first();
        $transacao->update([
            "status" => $aprovado->transaction_status_uid,
            "response_hash" => $hashResponse
        ]);
    }
    public function erroTransacao($transacao_uid, $message)
    {
        $transacao = $this->findById($transacao_uid);

        if($transacao){
            $error = TransacaoTipo::where('name', 'error')->first();
            $transacao->update([
                "status" => $error->transaction_status_uid,
                "status_description" => $message
            ]);
            return true;
        }
        return false;
    }

}
