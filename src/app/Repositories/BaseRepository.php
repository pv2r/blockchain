<?php


namespace App\Repositories;


use App\Messages\Messages;
use Illuminate\Http\Request;
use App\Interfaces\IRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class BaseRepository implements IRepository
{
    protected $model;
    protected $perPage;
    use Messages;

    public function __construct(string $model, int $perPage = 10)
    {
        $this->model = $model;
        $this->perPage = $perPage;
    }


    public function store(array $dados)
    {
        return $this->model::create($dados);
    }

    public function update(string $uid, array $dados)
    {
        $model = $this->findById($uid);

        if (!$model) {
            return null;
        }

        $model->fill($dados);
        $model->update();

        return $model;
    }

    public function destroy(string $uid, string $deleted_by)
    {
        $model = $this->findById($uid);

        if (!$model) {
            return null;
        }

        $model->deleted_by = $deleted_by;
        $model->save();

        return $this->model::destroy($uid);
    }

    public function findById(string $uid)
    {
        return $this->model::find($uid);
    }

    public function filter(array $search, array $searchWith)
    {
        $allowedInArray = ['related', 'related_field', 'related_value', 'with_paginate'];
        $model = new $this->model;

        //incluindo campos personalizaveis na busca
        $fieldsSearch = array_merge(['created_at', $model->getKeyName()], $model->getFillable());

        Log::info('fields search', [$fieldsSearch]);

        //validando campos permitidos
        foreach ($search as $key => $value) {
            if (in_array($key, $allowedInArray)) {
                continue;
            }

            if (!array_keys($fieldsSearch, $key)) {
                unset($search[$key]);
            }
        }

        //retornando busca vazia
        if (empty($search)) {

            $resultIfEmptySearch = $model::orderBy('created_at', 'desc');

            if (!empty($searchWith)) {
                foreach ($searchWith as $item) {
                    $resultIfEmptySearch->with($item);
                }
            }

            if (isset($search['with_paginate']) && (bool)$search['with_paginate'] == true) {
                return $resultIfEmptySearch->paginate($this->perPage);
            }

            return $resultIfEmptySearch->get();
        }

        $result = $model::query();

        //filtro para data
        if (array_key_exists('created_at', $search)) {
            $date = $search['created_at'];
            unset($search['created_at']);

            $result->whereDate('created_at', $date);
        }

        //percorrendo os campos enviados e criando um where
        foreach ($search as $column => $value) {
            if (in_array($column, $allowedInArray)) {
                continue;
            }
            if (is_array($value)) {
                $result->whereIn($column, $value);
                continue;
            }
            $result->where($column, 'LIKE', "%{$value}%");
        }

        $related = $search['related'] ?? '';
        $relatedField = $search['related_field'] ?? '';
        $relatedValue = $search['related_value'] ?? '';
        $relatedActive = $search['related_active'] ?? false;

        if (!empty($related) && !empty($relatedField)) {
            $result->whereHas($related, function ($query) use ($relatedField, $relatedValue) {
                $query->where($relatedField, 'LIKE', "%{$relatedValue}%");
            });
        }

        if (!empty($related) && $relatedActive) {
            $result->whereHas($related, function ($query) use ($relatedField, $relatedValue) {
                $query->whereNull('deleted_at');
            });
        }

        if (!empty($searchWith)) {
            foreach ($searchWith as $item) {
                $result->with($item);
            }
        }

        $result->orderBy('created_at', 'desc');

        if (isset($search['with_paginate']) && (bool)$search['with_paginate'] == true) {
            return $result->paginate($this->perPage);
        }

        return $result->get();
    }

    public function getAll()
    {
        return $this->model::all();
    }

    public function getAllPaginated(int $perPage)
    {
        return $this->model::paginate($perPage);
    }

    public function advancedFilter(Request $request)
    {
        $where = $request->has('where') ? $request->get('where') : [];
        $joins = $request->has('join') ? $request->get('join') : [];
        $selects = $request->has('select') ? $request->get('select') : ["*"];
        $orderBy = $request->has('orderBy') ? $request->get('orderBy') : [];
        $groupBy = $request->has('groupBy') ? $request->get('groupBy') : [];

        // configuracoes basicas do modelo e tabela
        $model = new $this->model;
        $tableDefault = $model->getTable();

        // iniciando query
        $query = $model::query();
        if (!empty($joins)) {
            foreach ($joins as $join) {
                $query->{$join['type']}($join['table'], $join['from'], $join['operation'], $join['to']);
            }
        }

        foreach ($where as $item) {
            $explodeColumn = explode('.', $item['column']);
            $tableAndColumn = $item['column'];
            if (count($explodeColumn) == 1) {
                $tableAndColumn = "{$item['table']}.{$item['column']}";
            }

            switch ($item['type']) {
                case "where":
                    $query->where("{$tableAndColumn}", $item['operation'], $item['value']);
                    break;
                case "orWhere":
                    $query->orWhere("{$tableAndColumn}", $item['operation'], $item['value']);
                    break;
                case "whereNull":
                    $query->whereNull("{$tableAndColumn}");
                    break;
                case "whereNotNull":
                    $query->whereNotNull("{$tableAndColumn}");
                    break;
                case "whereIn":
                    $query->whereIn("{$tableAndColumn}", $item['value']);
                    break;
                default:
                    throw new \Exception('Unexpected value type');
            }
        }

        foreach ($selects as $select) {
            if ($select != '*') {
                $explode = explode('.', $select);
                if (count($explode) == 1) {
                    $select = "{$tableDefault}.{$select}";
                }
            }
            $query->addSelect($select);
        }

        if (!empty($orderBy) && isset($orderBy['column'])) {
            $query->orderBy($orderBy['column'], isset($orderBy['type']) ? $orderBy['type'] : 'DESC');
        }

        if (Schema::hasColumn($tableDefault, 'deleted_at')) {
            $query->whereNull("{$tableDefault}.deleted_at");
        }

        $query->orderByDesc("{$tableDefault}.created_at");

        if (!empty($groupBy) && isset($groupBy['column'])) {
            $query->groupBy($groupBy['column']);
        }

        return $query;
    }

    protected function createWithRelations(object $model, array $relacionamentos)
    {
        foreach ($relacionamentos as $key => $data) {
            $model->$key()->associate($data);
        }
        $model->save();
        return $model;
    }
}
