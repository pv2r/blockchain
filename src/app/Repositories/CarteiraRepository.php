<?php

namespace App\Repositories;

use App\Models\Carteira;
use App\Messages\Messages;
use Illuminate\Http\Request;
use App\Services\CarteiraService;
use Illuminate\Support\Facades\Log;
use App\Repositories\BaseRepository;
use App\Others\Contracts\RepositoryAbstract;

class CarteiraRepository extends BaseRepository
{
    private $carteiraService;
    private $perfilRepository;
    private $categoriaRepository;
    private $frequenciaRepository;
    protected $model;

    public function __construct($model, $perpage = 10)
    {
        parent::__construct(__CLASS__, $perpage = 10);
        $this->carteiraService = new CarteiraService();
        $this->perfilRepository = new PerfilRepository();
        $this->categoriaRepository = new CategoriaRepository();
        $this->frequenciaRepository = new FrequenciaRepository();
        $this->algoritmoRepository = new AlgoritmoRepository();
        $this->model = $model;
        return $this;
    }

    public function findByAddress(string $address, array $searchWith = [])
    {
        return $this->model::where('address', $address)->with($searchWith)->first();
    }

    public function store(array $request)
    {
        try {
            list($perfil, $categoria, $frequencia, $algoritmo) = $this->getRelacionamentos($request);

            $dadosDaCarteira = $this->carteiraService->create($request['algorithm_uid']);

            if ($perfil && $categoria && $frequencia && $dadosDaCarteira && $algoritmo) {
                $carteiraModel = [
                    'description' => $request['description'],
                    'account_uid' => $request['account_uid'] ?? null,
                    'address' => $dadosDaCarteira->data->endereco,
                    'public_key' => $dadosDaCarteira->data->chave_publica,
                    //mock
                    'created_by' => 'Fintools'
                ];

                $newModel = new Carteira($carteiraModel);
                $relacionamentos = [
                    "profile" => $perfil,
                    "category" => $categoria,
                    "frequency" => $frequencia,
                    "algorithm" => $algoritmo
                ];

                $newModel = $this->createWithRelations($newModel, $relacionamentos);

                return $newModel->only(['address', 'public_key']);
            }
            return false;
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception($this->message('falha.interna.criar.dados'), 400);
            return false;
        }
    }

    //    public function index(String $network_uid = null, int $perPage = 15)
    //    {
    //        if (!is_null($network_uid)) {
    //            $carteiras = $this->model::with(['networks', 'profile', 'category', 'frequency'])->whereHas('networks', function ($q) use ($network_uid) {
    //                $q->where('network_uid', '=', $network_uid);
    //            })->paginate($perPage);
    //            return $carteiras;
    //        }
    //        $carteiras = $this->model::with(['networks', 'profile', 'category', 'frequency'])->paginate($perPage);
    //        return $carteiras;
    //
    //    }

    public function searchPublicKey(array $attributes, array $only = [])
    {

        $carteira = $this->model::with(['algorithm'])->where('address', $attributes['address'])->first();
        if (is_null($carteira)) {
            throw new \Exception("Unable to recover public key", 400);
        }
        if (empty($only)) {
            return $carteira;
        }
        return $carteira->only($only);
    }

    /**
     * @param array $request
     * @return array
     * @throws \Exception
     */
    private function getRelacionamentos(array $request): array
    {
        $perfil = $this->perfilRepository
            ->findBy('profile_uid', $request['profile_uid'])
            ->first() ?? false;
        $categoria = $this->categoriaRepository
            ->findBy('category_uid', $request['category_uid'])
            ->first() ?? false;
        $frequencia = $this->frequenciaRepository
            ->findBy('frequency_uid', $request['frequency_uid'])
            ->first() ?? false;

        $algoritmo = $this->algoritmoRepository
            ->findBy('algorithm_uid', $request['algorithm_uid'])
            ->first() ?? false;

        return array($perfil, $categoria, $frequencia, $algoritmo);
    }
}
