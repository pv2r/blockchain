<?php

namespace App\Repositories;

use App\Others\Contracts\RepositoryAbstract;


class FrequenciaRepository extends RepositoryAbstract
{

    public function __construct()
    {
        parent::__construct(__CLASS__);
        return $this;
    }
}