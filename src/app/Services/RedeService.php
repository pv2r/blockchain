<?php

namespace App\Services;


use App\Models\Rede;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Exceptions\ServiceRequestException;



class RedeService extends BaseService
{
    private $created_by;

    public function __construct($baseRoute = null)
    {
        parent::__construct();
        $this->baseUrl = config('custom.SERVICE_URL_BLOCKCHAIN');
        $this->baseRoute = $baseRoute;
        //mock
        $this->created_by = 'Fintools';
    }

    /**
     * @return mixed
     * @throws ServiceRequestException
     */
    public function getAll(): mixed
    {
        $this->setRoute('/redes');
        $response = Http::get($this->url());
        if($response->failed()){
            Log::error("erro ao consumir url: {$this->url()} error: ".json_encode($response->json()));
            throw new ServiceRequestException();
        }
        return $response->json();
    }

    public function store(Rede $rede)
    {
        $url = $this->url().'/redes';
        
        $rede_tratada = $this->filtrarRede($rede->getAttributes());
        $resultRequest = Http::post($url,$rede_tratada);
        if($resultRequest->successful()){
            return $resultRequest->object();
        }
            throw new \Exception("Network could not be created", 400);

    }
    public function delete(String $rede_uid)
    {
        $url = $this->url().'/redes';
        $data = [
            "rede_uid" => $rede_uid
        ];
        $resultRequest = Http::delete($url,$data);
        if($resultRequest->successful()){
            return $resultRequest->object();
        }
            throw new \Exception("Network could not be deleted", 400);

    }
    
    private function filtrarRede(Array $rede)
    {
        $rede_tratada = [
            "nome" => $rede["name"],
            "rede_uid" => $rede["network_uid"]->serialize(),
            "nome_exibicao" => $rede["label"],
            "moeda_principal" => $rede["coin"],
            "provider" => $rede["provider"],
            "algoritmo_uid" => $rede["algorithm_uid"],
            "created_by" => $rede["created_by"],
            "updated_by" => $rede["updated_by"]
        ];
        return $rede_tratada;
    } 
}
