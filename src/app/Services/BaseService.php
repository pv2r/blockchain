<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class BaseService
{
    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $baseRoute;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    private $route;

    /**
     * BaseService constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'http_errors' => false,
            'base_uri' => $this->baseUrl,
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);
    }

    /**
     * @param string $name
     * @return self
     */
    public function setRoute(string $name): self
    {
        $this->route = $name;

        return $this;
    }

    /**
     * @param string $url
     * @return self
     */
    protected function setBaseUrl(string $url): self
    {
        $this->baseUrl = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    protected function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @return string|null
     */
    protected function getBaseRoute(): ?string
    {
        return $this->baseRoute;
    }

    /**
     * @param string|null $uri
     * @param string|null $params
     * @return string
     */
    protected function url(string $uri = null, string $params = null)
    {
        return $this->baseUrl . $this->baseRoute . $this->getRoute() . $uri . $params;
    }

    /**
     * @param string $method
     * @param string|null $uri
     * @param array $sendData
     * @param null $file
     * @return string
     * @throws GuzzleException
     */
    protected function request(string $method, string $uri = null, array $sendData = [], $file = null)
    {
        $data = [];
        Log::info('baseUrl', [$this->baseUrl]);
        $request = new \GuzzleHttp\Psr7\Request($method, $uri);
        if (!empty($sendData)) {
            $data['json'] = $sendData;
            Log::info('Log de request:', [$method, $uri, $data]);
        }

        if (!empty($file)) {
            $data = [];
            $multiData = [];

            foreach ($sendData as $name => $content) {
                $multiData['multipart'][] = ['name' => $name, 'contents' => $content];
            }
            $multiData['multipart'][] = [
                'Content-type' => 'multipart/form-data',
                'name' => 'file',
                'filename' => $file->getClientOriginalName(),
                'Mime-Type' => $file->getMimeType(),
                'contents' => fopen($file->getPathName(), 'r')
            ];

            $multipart = new \GuzzleHttp\Psr7\MultipartStream($multiData['multipart']);
            $request = new \GuzzleHttp\Psr7\Request($method, $uri);
            $request = $request->withBody($multipart);
            Log::info('Log de request:', [$method, $uri, $multiData]);
        }

        if (empty($data)) {
            $data['form-params'] = [];
        }
        //$request = $this->client->request($method, $uri, $data);
        $request = $response = $this->client->send($request, $data);

        $logData = [
            'url' => $uri,
            'data' => $data,
            'statusCode' => $request->getStatusCode()
        ];

        if (!in_array($request->getStatusCode(), ['200'])) {
            Log::warning('Error in access url', $logData);
        } else {
            Log::info('Success in access url', $logData);
        }

        return $request->getBody()->getContents();
    }
}
