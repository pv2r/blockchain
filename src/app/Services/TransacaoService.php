<?php

namespace App\Services;


use App\Models\Transacao;
use App\Repositories\TransacaoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use MongoDB\Driver\Exception\Exception;

class TransacaoService extends BaseService
{
    private $created_by;
    private $repository;

    public function __construct($baseRoute = null)
    {
        parent::__construct();
        $this->baseUrl = config('custom.SERVICE_URL_BLOCKCHAIN');
        $this->baseRoute = $baseRoute;
        $this->repository = new TransacaoRepository(Transacao::class);
        //mock
        $this->created_by = 'Fintools';
    }

    /**
     * @param Transacao $transacao
     * @return array|mixed
     * @throws \Exception
     */
    public function create(Transacao $transacao)
    {
        $this->setRoute("/blocos");
        $postTransacao = $transacao->getAttributes();
        $response = Http::post($this->url(), $this->filtrarTransacao($postTransacao));
        if($response->failed()){
            Log::error("erro ao consumir url: {$this->url()} error: ".json_encode($response->json()));
            throw new \Exception($response->object()->message, 400);
        }
        return $response->object();
    }

    public function sign(Transacao $transacao)
    {
        $this->setRoute("/transacao/assinar");
        $postTransacao = $transacao->getAttributes();
        $response = Http::post($this->url(), $this->filtrarTransacao($postTransacao));
        if($response->failed()){
            Log::error("erro ao consumir url: {$this->url()} error: ".json_encode($response->json()));
            throw new \Exception($response->object()->message, 400);
        }
        return $response->object();
    }

    private function filtrarTransacao(Array $postTransacao)
    {
        return [
            "network_uid" => $postTransacao["network_uid"],
            "to" => $postTransacao["to_address"],
            "from" => $postTransacao["from_address"],
            "value" => $postTransacao["value"],
            "data" => $postTransacao["data"]
        ];
    }

}
