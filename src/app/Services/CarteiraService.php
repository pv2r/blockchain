<?php

namespace App\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use MongoDB\Driver\Exception\Exception;

class CarteiraService extends BaseService
{
    private $created_by;

    public function __construct($baseRoute = null)
    {
        parent::__construct();
        $this->baseUrl = config('custom.SERVICE_URL_BLOCKCHAIN');
        $this->baseRoute = $baseRoute;
        //mock
        $this->created_by = 'Fintools';
    }

    public function create(string $algoritmo)
    {
        //mock
        $data = [
            'created_by' => $this->created_by,
            'algoritmo' => $algoritmo
        ];
        $url = $this->baseUrl . '/carteiras';
        try {
            $carteira = $this->request('POST', $url, $data) ?? null;
            if (isset($carteira)) {
                return json_decode($carteira);
            }
            return false;
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }

    public function getChavePublica(string $endereco, string $rede)
    {
        //mock
        $data = [
            'created_by' => $this->created_by,
            'endereco' => $endereco,
            'rede' => $rede
        ];
        $url = $this->baseUrl . '/carteiras/chave_publica';

        $carteira = $this->request('GET', $url, $data) ?? null;
        $carteira = json_decode($carteira);

        return $carteira->data ?? [];
    }
}
