<?php

namespace App\Others\Contracts;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

abstract class RepositoryAbstract implements RepositoryInterface
{
    protected $model, $msgError, $order, $limit;
    private $statusCode = 400;
    private $perPage = 20;
    private $selectFields = [];
    private $whereFields = [];
    private $whereInFields = [];
    private $with = [];
    private $join = [];
    private $scope = [];
    private $groupBy = null;

    private $uniqueFields = false;
    private $resultInArray = false;
    private $onlyCount = false;
    private $forceFilter = false;
    private $forceBindModel = false;
    private $semPaginacao = false;
    private $baseFiltros = [];

    const DELETE = 0;
    const CREATE = 1;
    const UPDATE = 2;

    function __construct($modelBind, $model = null)
    {
        if (!is_object($modelBind)) {
            $modelName = str_replace('Repository', '', last(explode('\\', $modelBind)));
            $modelBind = $this->factory($modelName);
        }
        if (!$model instanceof $modelBind) {
            $model = new $modelBind();
        }
        if (is_object($model)) {
            $this->model = $model;
        }
    }

    public function getTable(): string
    {
        return $this->getModel()->getTable();
    }

    public function getRelated($name, $instanceModel = false)
    {
        return !$instanceModel ? $this->getModel()->{$name} : $this->getModel()->{$name}()->getRelated();
    }

    public function joinStatus(string $statusNome = null, string $alias = null)
    {
        $tblMain = $this->getModel()->getTable();
        $tblStatus = 'status';
        $aliasTmp = (empty($alias) ? '' : ' as ' . $alias);
        $this->setJoin($tblStatus . $aliasTmp, function ($join) use ($tblStatus, $tblMain, $statusNome, $alias) {
            $join->on($tblMain . '.status_uid', '=', ($alias ? $alias : $tblStatus) . '.uid');
            if ($statusNome) {
                $join->where(($alias ? $alias : $tblStatus) . '.nome', $statusNome);
            }
        });
        return $this;
    }

    /**
     * Seta current Model in repository
     * @param $instanceModel
     * @param false $force
     * @return $this
     */
    public function bindModel($instanceModel, $force = false)
    {

        if ($instanceModel instanceof $this->model || $force === true) {
            $this->model = $instanceModel;
        } else if (
            method_exists($instanceModel, 'getModel') &&
            $instanceModel->getModel() instanceof $this->model
        ) {
            $this->model = $instanceModel;
        }
        $this->forceBindModel = false;
        $this->resetWhere();
        return $this;
    }

    public function propEmpty($property)
    {
        return empty($this->get($property));
    }

    public function set($campo, $valor)
    {
        $this->model->$campo = $valor;
        return $this;
    }

    public function setByArray($array)
    {
        foreach ($array as $key => $value) {
            $this->set($key, $value);
        }
        return $this;
    }

    public function setReturnArray($boolean = true)
    {
        $this->resultInArray = $boolean;
        return $this;
    }

    public function setLimit(Int $value)
    {
        $this->limit = (int)$value;
        return $this;
    }

    public function setSelectUnique($values)
    {
        $this->setSelect($values);
        $this->uniqueFields = TRUE;
        return $this;
    }

    public function setScope($name)
    {
        $this->scope[] = $name;
        return $this;
    }

    public function setPerPage(int $perPage)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * segue o padrao daqui: config('filters.search')
     * @param array $filtros
     * @return $this
     */
    public function setbaseFiltros(array $filtros)
    {
        $this->baseFiltros = $filtros;
        return $this;
    }

    public function setJoin($related, $queryFunction, $type = 'join')
    {
        $this->join[] = ['related' => $related, 'type' => $type, 'query' => $queryFunction];
        return $this;
    }

    public function setSelect($values)
    {
        if (!$this->uniqueFields && !empty($values)) {
            $values = !is_array($values) ? explode(',', $values) : $values;
            foreach ($values as $row) {
                $row = trim($row);
                if (!in_array($row, $this->selectFields)) {
                    $this->selectFields[] = $row;
                }
            }
        }
        return $this;
    }

    public function setWhere($values, $operator = '', $explode = true)
    {
        if (!empty($values)) {
            if (empty($operator) && !empty($this->whereFields)) {
                $operator = 'and';
            }
            if ($explode) {
                $values = !is_array($values) ? explode(',', $values) : $values;
            } else {
                $values = [$values];
            }
            foreach ($values as $row) {
                $operator = empty($operator) ? '' : trim($operator);
                $this->whereFields[] = trim("$operator ($row)");
            }
        }
        return $this;
    }

    public function setWhereRaw($values, $operator = '')
    {
        if (!empty($values)) {
            if (empty($operator) && !empty($this->whereFields)) {
                $operator = 'and';
            }
            $operator = empty($operator) ? '' : trim($operator);
            $this->whereFields[] = trim("$operator ($values)");
        }
        return $this;
    }

    public function setGroupBy(string $text)
    {
        $this->groupBy = $text;
        return $this;
    }

    public function setMultiFilter($field, $value, $operator = 'or')
    {
        $values = !is_array($value) ? [$value] : $value;
        $where = '';
        foreach ($values as $row) {
            $where .= "($field='$row') $operator ";
        }
        $rtrim = rtrim($where, "$operator ");
        $this->setWhereRaw($rtrim);
        return $this;
    }

    public function setOrderBy($orderRaw)
    {
        $this->order = $orderRaw;
        return $this;
    }

    public function setMsgError($msg, $statusCode = 400)
    {
        $this->msgError = $msg;
        $this->statusCode = $statusCode;
        return $this;
    }

    public function resetMsgError()
    {
        $this->msgError = null;
        return $this;
    }

    public function get($campo, $returnDefault = FALSE)
    {
        return (isset($this->model->$campo) ? $this->model->$campo : $returnDefault);
    }

    /**
     * @return false|mixed
     */
    public function save()
    {
        $msg = null;
        try {
            $return = $this->model->save();
            $this->msgError = null;
        } catch (\Exception $e) {
            $return = false;
            $msg = $e->getMessage();
        }
        if (!$return) {
            //dd($msg);
            //send_log($msg, $this->model->toArray(), 'error');
            $this->setMsgError($msg, 500);
        }

        return $return;
    }

    /**
     * @param array $dados
     * @return false|mixed
     */
    public function update(array $dados)
    {
        $msg = null;
        try {
            $return = $this->model->update($dados);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $this->setMsgError($msg, 500);
            //send_log($msg, $dados, 'error');
            $return = false;
        }
        return $return;
    }

    /**
     * @param array $lista
     * @return false|mixed
     */
    public function create(array $lista)
    {
        $msg = null;
        try {
            $return = $this->model->create($lista);
            $this->bindModel($return);
        } catch (\Exception $e) {
            $return = false;
            $msg = $e->getMessage();
            $this->setMsgError($msg, 500);
            Log::error('FALHAR CRIACAO: ' . $e->getMessage() . " / " . $lista);
        }

        return $return;
    }

    /**
     * @param array $matrizLista
     * @return false|mixed
     */
    public function createInLote(array $matrizLista)
    {
        $msg = null;
        try {
            $this->model->insert($matrizLista);
            $return = true;
        } catch (\Exception $e) {
            $return = false;
            $msg = $e->getMessage();
            $this->setMsgError($msg, 500);
            Log::error('FALHAR CRIACAO: ' . $e->getMessage() . " / " . $matrizLista);
        }

        return $return;
    }

    /**
     * DELETA REGISTROS<br>
     * $verifyBefore = é uma function que deverá retornar sempre true ou false, indicando se o delete deve ou nao seguir
     * @param $key
     * @param string $uk
     * @param null|object $verifyBefore
     * @return false
     */
    public function delete($key, $uk = 'uid', object $verifyBefore = null)
    {
        $return = FALSE;
        $collection = $this->getModel()->where($uk, $key)->first();
        if (!empty($collection)) {
            // USADO PARA O $collection dentro do $verifyBefore ser reconhecido pelo getModel()
            $this->bindModel($collection);

            if (is_callable($verifyBefore) && !$verifyBefore()) {
                return false;
            }

            $return = $collection->delete();
            if (!$return) {
                Log::error(trans('default.delete_yes'));
                $this->setMsgError(trans('default.delete_yes'));
            }
        } else {
            Log::error(trans('default.register_not_found'));
            $this->setMsgError(trans('default.register_not_found'), 404);
        }
        return $return;
    }

    public function getLastID($key = 'id')
    {
        return (isset($this->model->{$key}) ? $this->model->{$key} : FALSE);
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    public function searchOrPaginate()
    {

        $paginado = !request()->query->has('sem_paginacao');
        if ($this->semPaginacao === true) {
            $paginado = false;
            $this->semPaginacao = false;
        }

        return $paginado ? $this->paginate() : $this->search();
    }

    public function search()
    {
        $querie = $this->processWhereAndFields();
        return !$this->isOnlyCount() ? $querie->get()->toArray() : $querie->count();
    }

    /**
     * @param int|null $perPage
     * @param int|null $page
     * @return mixed
     * @throws \Exception
     */
    public function paginate(int $perPage = null, int $page = null)
    {
        $perPage = $perPage ? $perPage : $this->perPage;
        return $this->processWhereAndFields()->paginate($perPage, '*', 'page', $page)->toArray();
    }

    public function first()
    {
        $querie = $this->processWhereAndFields();
        $result = $querie->first();

        if ($result && $this->forceBindModel) {
            $this->bindModel($result);
        }

        return $result ? (!$this->resultInArray ? $result : $result->toArray()) : (!$this->resultInArray ? null : []);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    private function processFilters()
    {

        foreach ($this->baseFiltros as $key => $search) {
            $valor = request()->query->get($key);
            if ($valor || is_numeric($valor) || is_bool($valor)) {
                if (!is_array($search)) {

                    if ($this->forceFilter || in_array($key, $this->getModel()->getFillable())) {
                        if (Str::contains($search, 'in')) {
                            $this->setWhereIn($search, [], $valor);
                        } else {
                            $this->setWhere(str_replace(config('filters.replace'), $valor, $search));
                        }
                    } else {
                        throw new \Exception(trans('default.search_parameters_incorrets'));
                    }
                } else {
                    list($with, $where) = $search;
                    $this->withHas($with, [
                        'filterRaw' => str_replace(config('filters.replace'), request()->query->get($key), $where)
                    ]);
                }
            }
        }
        return $this;
    }

    /**
     * @param null $forceModel
     * @return Model|null
     * @throws \Exception
     */
    protected function processWhereAndFields($forceModel = null)
    {

        if (empty($forceModel)) {
            $this->processFilters();
            $model = $this->model;
        } else {
            $model = $forceModel;
        }

        if ($this->whereFields) {
            $model = $model->whereRaw(implode(' ', $this->whereFields));
        }
        if ($this->whereInFields) {
            $model = empty($this->whereInFields['raw']) ?
                $model->whereIn($this->whereInFields['key'], $this->whereInFields['array']) :
                $model->whereRaw($this->whereInFields['raw']);
        }
        if (!empty($this->getSelectFields())) {
            $fieldsRaw = implode(', ', $this->getSelectFields(true));
            $model = $model->selectRaw($fieldsRaw);
            $this->uniqueFields = FALSE;
            $this->selectFields = [];
        }
        if ($this->with) {
            foreach ($this->with as $with) {
                $fields = is_array($with['fields']) ? $with['fields'] : explode(',', str_replace(', ', ',', $with['fields']));
                $filterRaw = empty($with['filterRaw']) ? null : $with['filterRaw'];
                $withPivot = empty($with['withPivot']) ? null : $with['withPivot'];
                $withCount = empty($with['withCount']) ? null : $with['withCount'];

                if ($withCount !== true) {
                    $model = $model->with([$with['name'] => function ($query) use ($fields, $filterRaw, $withPivot) {
                        if (!empty($fields) && is_array($fields)) {
                            $query->select($fields);
                        }
                        if (!empty($filterRaw)) {
                            $query->whereRaw($filterRaw);
                        }
                        if (!empty($withPivot)) {
                            $query->withPivot($withPivot);
                        }
                    }]);
                } else {
                    $model = $model->withCount([
                        $with['name'] => function ($query) use ($fields, $filterRaw) {
                            if (!empty($fields) && is_array($fields)) {
                                $query->select($fields);
                            }
                            if (!empty($filterRaw)) {
                                $query->whereRaw($filterRaw);
                            }
                        }
                    ]);
                }
            }
            $this->with = [];
        }
        if ($this->scope) {
            foreach ($this->scope as $scope) {
                $model = $model->{$scope}();
            }
        }
        if ($this->join) {
            foreach ($this->join as $join) {
                $model = $model->{$join['type']}($join['related'], $join['query']);
            }
            $this->join = [];
        }
        if ($this->limit) {
            $model = $model->limit($this->limit);
            $this->limit = null;
        }
        if ($this->groupBy) {
            $model = $model->groupByRaw($this->groupBy);
            $this->order = null;
        }
        if ($this->order) {
            $model = $model->orderByRaw($this->order);
            $this->order = null;
        }
        return $model;
    }

    /**
     * Busca pela PK e seta a model atual
     * @param $valor
     * @return $this
     */
    public function find($valor)
    {
        $model = $this->processWhereAndFields();
        $result = $model->find($valor);
        if (!is_null($result)) {
            $this->model = $result;
        } else {
            $this->setMsgError(trans('default.register_not_found'));
        }
        return $this;
    }

    /**
     * @param string $campo
     * @return mixed
     */
    public function increment(string $campo)
    {
        return $this->get($campo) != -1 ? $this->model->increment($campo) : null;
    }

    /**
     * @param string $campo
     * @return mixed
     */
    public function decrement(string $campo)
    {
        return $this->get($campo) > 0 ? $this->model->decrement($campo) : null;
    }

    /**
     * Busca registro informando campo unico. Se encontrado, seta model
     * @param $campoUnico
     * @param $documento
     * @return $this
     * @throws \Exception
     */
    public function findBy($campoUnico, $documento)
    {
        $buildQuery = $this->processWhereAndFields()->where($campoUnico, $documento);
        if (!$this->isOnlyCount()) {
            $result = $buildQuery->first();
            if (!is_null($result)) {
                $this->model = $result;
            } else {
                $this->setMsgError(trans('default.register_not_found'));
            }
            return $this;
        }
        return $buildQuery->count();
    }

    /**
     * @param $array
     * @param string $oper
     * @return $this
     */
    public function findByArray($array)
    {
        $buildQuery = $this->processWhereAndFields();
        foreach ($array as $field => $value) {
            $buildQuery->where($field, $value);
        }
        $result = $buildQuery->first();
        if (!is_null($result)) {
            $this->model = $result;
        }
        return $this;
    }

    public function findAll($campo, $documento)
    {
        $buildQuery = $this->processWhereAndFields()->where($campo, $documento);
        return !$this->isOnlyCount() ? $buildQuery->get()->toArray() : $buildQuery->count();
    }

    public function findIn($array, $key = 'id')
    {
        $buildQuery = $this->getModel()->whereIn($key, $array);
        $this->processSelectFields($buildQuery);
        return $buildQuery->get()->toArray();
    }

    public function setWhereIn(string $key, array $array, string $raw = null)
    {
        // tratando busca pelo filtro
        if ($raw) {
            $this->whereInFields['raw'] = str_replace(config('filters.replace'), implode("','", preg_split("/[,;]+/", $raw)), $key);
            return $this;
        }

        $this->whereInFields = ['key' => $key, 'array' => $array];
        return $this;
    }

    public function firstOrNew($value, $field = 'id')
    {
        $this->bindModel($this->getModel()->firstOrNew([$field => $value]));
        return $this;
    }

    protected function processSelectFields(&$buildQuery, $selectRaw = '*')
    {
        $selectRaw = $this->processAlias($selectRaw);
        if (is_array($selectRaw)) {
            $buildQuery->select($selectRaw);
        } else {
            $buildQuery->selectRaw($selectRaw);
        }
        $this->selectFields = [];
    }

    private function processAlias($selectRaw = '*')
    {
        $selectRaw = empty($this->selectFields) ? $selectRaw : $this->selectFields;
        //return $selectRaw;
        if ($selectRaw !== '*') {
            if (!is_array($selectRaw)) {
                $selectRaw = explode(',', $selectRaw);
            }
            foreach ($selectRaw as $key => $value) {
                $selectRaw[$key] = trim($value);
                /*if( strpos( $value, '.' ) === FALSE ){
                    $selectRaw[ $key ] = $this->getModel()->getTable().'.'.$value;
                }*/
            }
        }
        return $selectRaw;
    }

    public function all()
    {
        return $this->getModel()->all();
    }

    public function truncate()
    {
        return $this->getModel()->truncate();
    }

    public function toArray()
    {
        return $this->getModel()->toArray();
    }

    public function fails(string $column = null)
    {
        try {
            $return = empty(!$column ? $this->getModel()->getKey() : $this->get($column));
        } catch (\Exception $e) {
            $return = true;
            $this->setMsgError(trans('default.internal_server_error'));
        }
        return $return;
    }

    /**
     * DIZ SE HOUVE ALGUM ERRO NO PROCESSO. CONSIDERA A VARIAVEL 'MsgError' SETADA
     * @return bool
     */
    public function isError()
    {
        return !empty($this->getMsgError());
    }

    /**
     * DIZ SE TODOS OS PROCESSOS EXECUTADOS FORAM SEM ERROR. CONSIDERA A VARIAVEL 'MsgError' SETADA
     * @return bool
     */
    public function isOK()
    {
        return empty($this->getMsgError());
    }

    /**
     * VERIFICA SE A MODEL ESTA INSTANCIADA
     * @param string|null $column
     * @return bool
     */
    public function isOn(string $column = null)
    {
        return !$this->fails($column);
    }

    public function onlyCount()
    {
        $this->onlyCount = true;
        return $this;
    }

    /**
     * USADO PARA FORCAR QUE OS FILTROS NAO SEJAM APENAS O DEFINIDOS NO FILLABLE DA MODEL
     * @return $this
     */
    public function forceFilter()
    {
        $this->forceFilter = true;
        return $this;
    }

    public function forceBindModel()
    {
        $this->forceBindModel = true;
        return $this;
    }

    public function semPaginacao()
    {
        $this->semPaginacao = true;
        return $this;
    }

    private function factory($modelBind)
    {

        $factoryFind = [
            base_path('app' . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR) => 'App\Models\\',
        ];
        $instance = null;
        foreach ($factoryFind as $path => $namespace)
            if (file_exists($path . $modelBind . '.php')) {
                $instance = App::make($namespace . $modelBind);
            }

        if (empty($instance)) {
            throw new ApiException('Instancia Model ' . $modelBind . ' não encontrada');
        }

        return $instance;
    }

    public function startLog()
    {
        DB::enableQueryLog();
    }

    public function showLog()
    {
        return DB::getQueryLog();
    }

    public function getMsgError()
    {
        return $this->msgError;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getSelectFields($reset = false, $returnDefautl = null)
    {
        $return = $this->selectFields;
        if ($reset === true) $this->selectFields = null;
        return !empty($return) ? $return : $returnDefautl;
    }

    public function getWhere($reset = false)
    {
        $return = $this->whereFields;
        if ($reset === true) $this->resetWhere();
        return $return;
    }

    public function resetWhere()
    {
        $this->whereFields = [];
        return $this;
    }

    public function getOrderBy($reset = false)
    {
        $order = $this->order;
        if ($reset === true) $this->order = null;
        return $order;
    }

    public function getWith($name, $returnArray = false)
    {
        $result = $this->isOn() ? $this->getModel()->{$name} : null;
        return $result && $returnArray ? $result->toArray() : $result;
    }

    public function getBaseFiltros()
    {
        return $this->baseFiltros;
    }

    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * RETORNA VALOR E SETA PRA FALSE
     * @return bool
     */
    public function isOnlyCount()
    {
        $value = $this->onlyCount;
        $this->onlyCount = false;
        return $value;
    }

    /**
     * @param string $relatedName
     * @param array $arrFields
     * @param array $setup [ filterRaw | withPivot ]
     * @return $this
     */
    public function setWith(string $relatedName, array $arrFields = [], array $setup = [])
    {

        $tmp = [
            'name' => $relatedName,
            'fields' => $arrFields,
            'filterRaw' => Arr::get($setup, 'filterRaw', []),
            'withPivot' => Arr::get($setup, 'withPivot', []),
            'withCount' => Arr::gett($setup, 'withCount', false),
        ];
        $key = $this->searchLikeInArray($this->with, $relatedName);
        if ($key === false) {
            $this->with[] = $tmp;
        } else {
            $this->with[$key] = $tmp;
        }
        return $this;
    }

    /**
     * BUSCA APENAS SE EXISTIR PELO MENOS 1 REGISTRO NA RELACAO
     * @param $related
     * @param array $setup [ filterRaw | fields ]
     * @return $this
     */
    public function withHas($related, array $setup = [])
    {

        $this->bindModel($this->model->whereHas($related, function (Builder $query) use ($setup) {

            $filterRaw = Arr::get($setup, 'filterRaw', null);
            $fields = Arr::get($setup, 'fields', null);

            if (!empty($filterRaw)) {
                $query->whereRaw($filterRaw);
            }
            if (!empty($fields) && is_array($fields)) {
                $query->select($fields);
            }
        }));
        return $this;
    }

    public function doesnthave($related, array $setup = [])
    {
        $this->bindModel($this->model->doesnthave($related));
        return $this;
    }

    public function setFilter(&$where, $value = null, $operator = '')
    {
        $operator = empty($operator) ? (empty(trim($where)) ? '' : 'and') : $operator;
        $where .= rtrim(" $operator ( $value )", ' ');
        //dd( $where, $value );
        return $this;
    }

    public function setStatusTrue($operator = null, $alias = null, $directModel = false)
    {

        $operator = !empty($operator) ? $operator : '';

        $alias = empty($alias) ? '' : $alias . '.';
        if (!$directModel)
            $this->setWhere($alias . 'status=1', $operator);
        else
            $this->model = $this->model->where($alias . 'status', 1);

        return $this;
    }

    public function setStatusFalse($operator = null, $alias = null, $directModel = false)
    {
        $operator = !empty($operator) ? $operator : '';

        $alias = empty($alias) ? '' : $alias . '.';
        if (!$directModel)
            $this->setWhere($alias . 'status=0', $operator);
        else
            $this->model = $this->model->where($alias . 'status', 0);

        return $this;
    }

    private function setStatus($status, $operator = '')
    {
        $operator = empty($operator) ? (empty(trim($this->filter)) ? '' : 'and') : $operator;
        $str = "status=$status";
        $this->setFilter($str, $operator);
    }

    /**
     * Update tbl by unique column
     * @param $ukField
     * @param $valueField
     * @param $dataArray
     * @param bool $setUptAt
     * @param bool $forceValue
     * @return array
     */
    public function putByUkField($ukField, $valueField, $dataArray, $setUptAt = false, $forceValue = false)
    {
        $dados = null;
        $dataSend = [];

        #verificando se existe dados com valores para atualizar
        foreach ($dataArray as $key => $value) {
            if (!empty($value) || is_bool($valueField) || $value == '0' || $forceValue) {
                $dataSend[$key] = $value;
            }
        }

        if ($dataSend) {
            $return = $this->getModel()->where($ukField, $valueField)->update($dataSend);
            if (!empty($return)) {
                $dados = $return[0];
            }
        }
        return $dados;
    }

    public function putByFields($stringFilter, $dataSend, $fieldStringReturn = null)
    {
        $return = FALSE;
        if ($stringFilter) {
            $return = $this->getModel()->whereRaw($stringFilter)->update($dataSend);
            if ($return) {
                $return = TRUE;
            } else {
                $this->setMsgError('Erro ao atualizar registro');
            }
        }
        return $return;
    }

    public function store(array $dados)
    {
        return $this->create($dados);
    }

    public function getValidos(bool $paginado = null)
    {
        $paginado = is_null($paginado) ? false : (bool)$paginado;
        $this->setStatusTrue();
        return $paginado ? $this->paginate() : $this->search();
    }

    public function getSoftDelets(bool $paginado = null)
    {
        $paginado = is_null($paginado) ? false : (bool)$paginado;
        $this->setSoftDeleteTrue();
        return $paginado ? $this->paginate() : $this->search();
    }

    public function setSoftDeleteTrue()
    {
        $this->model = $this->model->onlyTrashed();
        return $this;
    }

    /**
     * @return false|mixed
     */
    public function restaurar()
    {
        $msg = null;
        try {
            $return = $this->model->restore();
        } catch (\Exception $e) {
            $return = false;
            $msg = $e->getMessage();
            Log::error($msg);
            Log::error($this->model->toArray());
            $this->setMsgError($msg, 500);
        }
        return $return;
    }

    private function searchLikeInArray($array, $search)
    {
        foreach ($array as $key => $value) {
            $current_key = $key;
            if ($search === $value or (is_array($value) && $this->searchLikeInArray($value, $search) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    protected function createWithRelations(object $model, array $relacionamentos)
    {
        foreach ($relacionamentos as $key => $data) {
            $model->$key()->associate($data);
        }
        $model->save();
        return $model;
    }
}
