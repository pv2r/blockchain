<?php

namespace App\Others\Contracts;

interface RepositoryInterface
{
    public function set($campo, $valor);

    public function get($campo);

    public function save();

    public function all();

    public function getLastID();

    public function getModel();

    public function bindModel($instanceModel);

    public function toArray();

    public function find($valor);

    public function increment(string $campo);

    public function decrement(string $campo);

    public function fails();
}
