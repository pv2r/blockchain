<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Testing\Fluent\Concerns\Has;

class Categoria extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $primaryKey = 'category_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'categories';

    protected $guarded = [];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "category_uid",
        "name"
    ];

    public function wallets()
    {
        return $this->hasMany(Carteira::class, 'category_uid', 'category_uid');
    }
}
