<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Frequencia extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $primaryKey = 'frequency_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'frequencies';

    protected $fillable = [
        'name',
        'label',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "frequency_uid",
        "name"
    ];

    public function wallets()
    {
        return $this->hasMany(Carteira::class, 'frequency_uid', 'frequency_uid');
    }
}
