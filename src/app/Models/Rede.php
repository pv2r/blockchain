<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rede extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $primaryKey = 'network_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'networks';
    protected $fillable = [
        'network_uid',
        'algorithm_uid',
        'name',
        'label',
        'coin',
        'provider',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function algorithm()
    {
        return $this->belongsTo(Algoritmo::class, 'algorithm_uid', 'algorithm_uid');
    }

}
