<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model
{
    use UuidTrait;
    use GlobalScopesTrait;
    use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'account_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'users';

    protected $fillable = [
        'name',
        'surname',
        'email',
        'document',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at"
    ];

    public function documentType()
    {
        return $this->belongsTo(InscricaoTipo::class, 'document_type', 'document_type_uid');
    }

    public function wallet()
    {
        return $this->hasone(Carteira::class, 'account_uid', 'account_uid');
    }

}
