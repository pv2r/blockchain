<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Algoritmo extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $table = 'algorithms';
    protected $primaryKey = 'algorithm_uid';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $guarded = [];

    protected $hidden = [
        "algorithm_uid",
        "name",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at"
    ];

    public function networks()
    {
        return $this->hasMany(Rede::class, 'algorithm_uid', 'algorithm_uid');
    }
}
