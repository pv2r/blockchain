<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InscricaoTipo extends Model
{
    use UuidTrait;
    use GlobalScopesTrait;
    use SoftDeletes;

    protected $primaryKey = 'document_type_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'document_types';

    protected $fillable = [
        'name',
        'label',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at"
    ];

    public function users()
    {
        return $this->hasMany(Usuarios::class, 'document_type_uid', 'document_type');
    }
}
