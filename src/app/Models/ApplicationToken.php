<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class ApplicationToken extends Model
{
    use HasApiTokens, Authenticatable, Authorizable;

    protected $guarded = [];
}
