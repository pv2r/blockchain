<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perfil extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $primaryKey = 'profile_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'profiles';

    protected $guarded = [];

    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "profile_uid",
        "name"
    ];

    public function wallets()
    {
        return $this->hasMany(Perfil::class, 'profile_uid', 'profile_uid');
    }
}
