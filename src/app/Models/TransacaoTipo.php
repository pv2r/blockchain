<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransacaoTipo extends Model
{
    use UuidTrait;
    use SoftDeletes;

    protected $primaryKey = 'transaction_status_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'transaction_status';

    protected $fillable = [
        'name',
        'label',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at"
    ];

    public function transacoes()
    {
        return $this->hasMany(Transacao::class, 'transaction_status_uid', 'status');
    }
}
