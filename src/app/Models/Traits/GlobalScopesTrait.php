<?php

namespace App\Models\Traits;

use App\Scopes\OrderByDescScope;

/**
 * Trait para centralizar global scopes
 */
trait GlobalScopesTrait
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderByDescScope);
    }
}
