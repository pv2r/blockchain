<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transacao extends Model
{
    use UuidTrait;
    use SoftDeletes;

    protected $primaryKey = 'transaction_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'transactions';

    protected $fillable = [
        'network_uid',
        'from_address',
        'to_address',
        'nonce',
        'gas_price',
        'gas_limit',
        'value',
        'data',
        'status',
        'status_description',
        'response_hash',
        'raw_transaction',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at"
    ];

    public function transaction_status()
    {
        return $this->belongsTo(TransacaoTipo::class, 'status', 'transaction_status_uid');
    }
}
