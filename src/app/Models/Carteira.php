<?php

namespace App\Models;

use App\Models\Traits\GlobalScopesTrait;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carteira extends Model
{
    use UuidTrait;
    use softDeletes;
    use HasFactory;

    protected $primaryKey = 'wallet_uid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'wallets';

    protected $guarded = [];

    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "algorithm_uid",
        "profile_uid",
        "category_uid",
        "frequency_uid",
        "account_uid",
        "public_key",
        "wallet_uid"
    ];

    public function user()
    {
        return $this->belongsTo(Usuario::class, 'account_uid', 'account_uid');
    }

    public function category()
    {
        return $this->belongsTo(Categoria::class, 'category_uid', 'category_uid');
    }

    public function frequency()
    {
        return $this->belongsTo(Frequencia::class, 'frequency_uid', 'frequency_uid');
    }

    public function profile()
    {
        return $this->belongsTo(Perfil::class, 'profile_uid', 'profile_uid');
    }

    public function algorithm()
    {
        return $this->belongsTo(Algoritmo::class, 'algorithm_uid', 'algorithm_uid');
    }


    public function categoria(): BelongsTo
    {
        return $this->belongsTo(Categoria::class, 'category_uid', 'category_uid');
    }

    public function frequencia(): BelongsTo
    {
        return $this->belongsTo(Frequencia::class, 'frequency_uid', 'frequency_uid');
    }

    public function perfil(): BelongsTo
    {
        return $this->belongsTo(Perfil::class, 'profile_uid', 'profile_uid');
    }

    public function algoritmo(): BelongsTo
    {
        return $this->belongsTo(Algoritmo::class, 'algorithm_uid', 'algorithm_uid');
    }
}
