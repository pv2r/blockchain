<?php

namespace App\Http\Requests;

class TransacaoRequest extends BaseRequest
{

    /**
     * Regras idênticas tanto na criação, quanto na edição
     * @Override
     * @return array
     */
    public static function commonRules(): array
    {
        return [
            'data.from_address' => 'string|max:42|exists:wallets,address|different:to_address',
            'data.to_address' => 'string|max:42|exists:wallets,address|different:from_address',
            'data.nonce' => 'string|max:180',
            'data.gas_price' => 'string|max:180',
            'data.gas_limit' => 'string|max:180',
            'data.value' => 'string|max:180',
            'data.data' => 'string'
        ];
    }
    /**
     * Regras para create
     * @Override
     * @return array
     */
    public static function createRules(): array
    {
        return [
            'network_uid' => 'required',
            'data.from_address' => 'required',
            'data.to_address' => 'required',
            'data.nonce' => 'required',
            'data.gas_price' => 'required',
            'data.gas_limit' => 'required',
            'data.value' => 'required',
            'data.data' => 'required'
        ];
    }
    public static function updateRules(): array
    {
        return [
            'network_uid' => 'required',
            'data.from_address' => 'required',
            'data.to_address' => 'required',
            'data.value' => 'required',
            'data.data' => 'required'
        ]; 
    }
    /**
     * Conversão de nomes
     * @return array
     */
    public static function fieldNames(): array
    {
        return [
        ];
    }

    /**
     * Retorna mensagens de erro personalizadas
     * @return array
     */
    public static function customMessages(): array
    {
        return [
            'data.from_address.required' => 'The from_address field is required.',
            'data.to_address.required' => 'The to_address field is required.',
            'network_uid.required' => 'The to_address field is required.',
            'data.nonce.required' => 'The nonce field is required.',
            'data.gas_price.required' => 'The gasPrice field is required.',
            'data.gas_limit.required' => 'The gasLimit field is required.',
            'data.value.required' => 'The value field is required.',
            'data.data.required' => 'The data field is required.',
            'data.from_address.exists' => 'The from_address sent does not appear in our database.',
            'data.to_address.exists' => 'The to_address sent does not appear in our database.',
            'data.to_address.different' => 'The wallets addresses must be differents.',
            'data.from_address.different' => 'The wallets addresses must be differents.'
        ];
    }

}
