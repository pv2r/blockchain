<?php

namespace App\Http\Requests\Transacao;

use App\Support\ResponseRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProcessarTransacaoRequest extends FormRequest
{
    /**
     * Disable validator redirect back to use in API
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        $response = (new ResponseRequest())->execute($validator->errors(), false, 'Internal failure to create data.', 422);
        throw new HttpResponseException($response);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'network_uid' => 'string|max:42',
            'data.from_address' => 'string|max:42|exists:wallets,address|different:to_address',
            'data.to_address' => 'string|max:42|exists:wallets,address|different:from_address',
            'data.nonce' => 'string|max:180',
            'data.gas_price' => 'string|max:180',
            'data.gas_limit' => 'string|max:180',
            'data.value' => 'string|max:180',
            'data.data' => 'string'
        ];
    }
}
