<?php

namespace App\Http\Requests\Carteira;

use App\Support\ResponseRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ListarCarteiraRequest extends FormRequest
{
    /**
     * Disable validator redirect back to use in API
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        $response = (new ResponseRequest())->execute($validator->errors(), false, 'Internal failure to create data.', 422);
        throw new HttpResponseException($response);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frequency_uid' => 'exists:frequencies,frequency_uid',
            'profile_uid' => 'exists:profiles,profile_uid',
            'category_uid' => 'exists:categories,category_uid',
            'algorithm_uid' => 'exists:algorithms,algorithm_uid'
        ];
    }
}
