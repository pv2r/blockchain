<?php

namespace App\Http\Requests;

class FrequenciaRequest extends BaseRequest
{

    /**
     * Regras idênticas tanto na criação, quanto na edição
     * @Override
     * @return array
     */
    public static function commonRules(): array
    {
        return [
            //mock
            'name' => 'required|string',
            'label' => 'required|string'
        ];
    }
    /**
     * Regras para create
     * @Override
     * @return array
     */
    public static function createRules(): array
    {
        return [

        ];
    }

    /**
     * Conversão de nomes
     * @return array
     */
    public static function fieldNames(): array
    {
        return [
            //mock
            'name' => 'Name',
            'label' => 'Label'

        ];
    }

    /**
     * Retorna mensagens de erro personalizadas
     * @return array
     */
    public static function customMessages(): array
    {
        return [
        ];
    }

}
