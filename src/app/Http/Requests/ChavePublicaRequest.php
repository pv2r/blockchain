<?php

namespace App\Http\Requests;


class ChavePublicaRequest extends BaseRequest
{
    /**
     * Regras idênticas tanto na criação, quanto na edição
     * @Override
     * @return array
     */
    public static function commonRules(): array
    {
        return [
            'address' => 'string|max:42|required|exists:wallets,address',
        ];
    }

    /**
     * Conversão de nomes
     * @return array
     */
    public static function fieldNames(): array
    {
        return [
            'address' => 'endereço'

        ];
    }

    /**
     * Retorna mensagens de erro personalizadas
     * @return array
     */
    public static function customMessages(): array
    {
        return [
            'address.required' => 'The address field is required.',
            'address.exists' => 'The address sent does not appear in our database.',
        ];
    }
}
