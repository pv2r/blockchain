<?php


namespace App\Http\Requests;


class BaseRequest
{
    /**
     * Regras idênticas tanto na criação, quanto na edição
     * @return array
     */
    public static function commonRules(): array
    {
        return [];
    }

    /**
     * Regras especificas de validação na criação
     * @return array
     */
    public static function createRules(): array
    {
        return [

        ];
    }

    /**
     * Regras especificas de validação na edição
     * @return array
     */
    public static function updateRules(): array
    {
        return [

        ];
    }

    /**
     * Regras de validação na deleção
     * @return array
     */
    public static function deleteRules(): array
    {
        return [
            'deleted_by' => 'required'
        ];
    }

    /**
     * Conversão de nomes
     * @return array
     */
    public static function fieldNames(): array
    {
        return [];
    }

    /**
     * Conversão de valores
     * @return array
     */
    public static function valuesNames(): array
    {
        return [];
    }

    /**
     * Retorna mensagens de erro personalizadas
     * @return array
     */
    public static function customMessages(): array
    {
        return [];
    }
}
