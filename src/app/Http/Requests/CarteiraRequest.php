<?php

namespace App\Http\Requests;

class CarteiraRequest extends BaseRequest
{

    /**
     * Regras idênticas tanto na criação, quanto na edição
     * @Override
     * @return array
     */
    public static function commonRules(): array
    {
        return [
            //mock
            'account_uid' => 'string|max:36',
            'description' => 'string|max:180',
            'frequency_uid' => 'exists:frequencies,frequency_uid',
            'profile_uid' => 'exists:profiles,profile_uid',
            'category_uid' => 'exists:categories,category_uid',
            'algorithm_uid' => 'exists:algorithms,algorithm_uid'
        ];
    }
    /**
     * Regras para create
     * @Override
     * @return array
     */
    public static function createRules(): array
    {
        return [
            'description' => 'required',
            'frequency_uid' => 'required',
            'profile_uid' => 'required',
            'category_uid' => 'required',
            'algorithm_uid' => 'required'
        ];
    }

    /**
     * Conversão de nomes
     * @return array
     */
    public static function fieldNames(): array
    {
        return [
            //mock
            'networks_uids' => 'redes',
            'description' => 'descrição',
            'frequency_uid' => 'frequencia',
            'profile_uid' => 'perfil',
            'category_uid' => 'categoria',

        ];
    }

    /**
     * Retorna mensagens de erro personalizadas
     * @return array
     */
    public static function customMessages(): array
    {
        return [
            'networks_uids.required' => 'The networks_uids field is required.',
            'description.required' => 'The description field is required.',
            'frequency_uid.required' => 'The frequency_uid field is required.',
            'profile_uid.required' => 'The profile_uid field is required.',
            'category_uid.required' => 'The category_uid field is required.',
            'algorithm_uid.required' => 'The algorithm_uid field is required.',
            'frequency_uid.exists' => 'The frequency_uid sent does not appear in our database.',
            'profile_uid.exists' => 'The profile_uid sent does not appear in our database.',
            'category_uid.exists' => 'The category_uid sent does not appear in our database.',
            'algorithm_uid.exists' => 'The algorithm_uid sent does not appear in our database.'
        ];
    }

}
