<?php

namespace App\Http\Controllers;

use App\Http\Requests\PerfilRequest;
use App\Models\Perfil;
use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;


class PerfilController extends BaseController
{
    public function __construct()
    {
        parent::__construct(Perfil::class, new BaseRepository(Perfil::class));
    }

    /**
     * @OA\Get(
     *     path="/api/v1/profiles",
     *     summary="Listagem de perfis",
     *     tags={"profiles"},
     *     description="Lista todas os perfis disponíveis",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function getAll(bool $returnJson = false)
    {
        $dataToShow = ['profile_uid'];
        $result = parent::getAll(false)->makeVisible($dataToShow);
        return $this->responseApi($result, true, $this->message('dados.retornados.sucesso'));
    }
}
