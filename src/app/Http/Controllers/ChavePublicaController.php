<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChavePublicaRequest;
use App\Models\Carteira;
use App\Repositories\CarteiraRepository;
use App\Services\CarteiraService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ChavePublicaController extends BaseController
{
    private CarteiraService $carteiraService;

    public function __construct(CarteiraService $carteiraService)
    {
        parent::__construct(
            Carteira::class,
            new CarteiraRepository(Carteira::class)
        );

        $this->carteiraService = $carteiraService;
        $this->carteiraService->setRoute('/carteiras');
    }

    /**
     * @OA\Get(
     *     path="/api/v1/wallets/public_key/address/{address_uid}",
     *     summary="Recuperação de chave pública",
     *     tags={"wallets"},
     *     description="Recupera a chave pública de uma carteira",
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="endereço da carteira",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function __invoke(String $address_uid)
    {
        try {

            $attributes = [
                'address' => $address_uid
            ];
            $public_key = $this->repository->searchPublicKey($attributes, ['public_key']);
            $resposta = $this->responseApi(
                $public_key,
                true,
                $this->message('dados.retornados.sucesso'),
                200
            );
        } catch (BadRequestException $e) {
            Log::error($e);
            $resposta = $this->responseApi([], false, $e->getMessage(), 400);
        } catch (ValidationException $e) {
            Log::error($e);
            return $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('erro.interno.realizar.busca'),
                $e->status
            );
        } catch (\Exception $e) {
            Log::error($e);
            $resposta = $this->responseApi([], false, $e->getMessage(), $e->getCode());
        }

        return $resposta;
    }
}
