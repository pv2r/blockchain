<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HealthCheckController extends Controller
{
    protected $name = "database";
    protected $description = "Checa a conexão com o servidor de banco de dados.";

    public function __invoke()
    {
        $response = [
            'project' => config('custom.PROJETO'),
            'healthy' => true,
            'checks' => [

            ]
        ];
        try{
            DB::connection()->getPdo();
            $status = [
                $this->name => [
                    "description" => $this->description,
                    "healthy" => true
                ]
            ];
            array_push($response['checks'], $status);
            return response()->json($response);
        }catch(\Exception $e){
            $status = [
                "database" => [
                    "description" => "Ocorreu um problema na conexão do banco de dados: " . $e->getMessage(),
                    "healthy" => false
                ]
            ];
            array_push($response['checks'], $status);
            $response['healthy'] = false;
            return response()->json($response);
        }


    }
}
