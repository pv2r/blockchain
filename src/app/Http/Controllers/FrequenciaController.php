<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrequenciaRequest;
use App\Models\Frequencia;
use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;


class FrequenciaController extends BaseController
{
    public function __construct()
    {
        parent::__construct(Frequencia::class, new BaseRepository(Frequencia::class));
    }

    /**
     * @OA\Get(
     *     path="/api/v1/frequencies",
     *     summary="Listagem de frequencias",
     *     tags={"frequencies"},
     *     description="Lista todas as frequencias disponíveis",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function getAll(bool $returnJson = false)
    {
        $dataToShow = ['frequency_uid'];
        $result = parent::getAll(false)->makeVisible($dataToShow);
        return $this->responseApi($result, true, $this->message('dados.retornados.sucesso'));
    }
}
