<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriaRequest;
use App\Models\Categoria;
use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;


class CategoriaController extends BaseController
{
    public function __construct()
    {
        parent::__construct(Categoria::class, new BaseRepository(Categoria::class));
    }

    /**
     * @OA\Get(
     *     path="/api/v1/categories",
     *     summary="Listagem de categorias",
     *     tags={"categories"},
     *     description="Lista todas as categorias disponíveis",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function getAll(bool $returnJson = false)
    {
        $dataToShow = ['category_uid'];
        $result = parent::getAll(false)->makeVisible($dataToShow);
        return $this->responseApi($result, true, $this->message('dados.retornados.sucesso'));
    }
}
