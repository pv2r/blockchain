<?php

namespace App\Http\Controllers;

use App\Http\Requests\Carteira\CriarCarteiraRequest;
use App\Http\Requests\Carteira\ListarCarteiraRequest;
use App\Models\Algoritmo;
use App\Models\Carteira;
use App\Services\CarteiraService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\CarteiraRepository;
use App\Http\Requests\CarteiraRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;


class CarteiraController extends BaseController
{
    public function __construct()
    {
        parent::__construct(
            Carteira::class,
            new CarteiraRepository(Carteira::class, 10)
        );
        $this->searchWith = ['algorithm', 'profile', 'category', 'frequency'];
    }

    /**
     * @OA\Post(
     *     path="/api/v1/wallets",
     *     summary="Criação de carteira",
     *     tags={"wallets"},
     *     description="Criação de carteira",
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Descrição da carteira",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="frequency_uid",
     *         in="query",
     *         description="ID da frequencia de uso da carteira.",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="profile_uid",
     *         in="query",
     *         description="ID do perfil da carteira.",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *      @OA\Parameter(
     *         name="category_uid",
     *         in="query",
     *         description="ID da categoria da carteira.",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *      @OA\Parameter(
     *         name="account_uid",
     *         in="query",
     *         description="ID da aplicação.",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *      @OA\Parameter(
     *         name="algorithm_uid",
     *         in="query",
     *         description="ID do algoritimo a ser implementado.",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="networks_uids",
     *         in="query",
     *         description="ID de redes compatíveis",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function criarCarteira(CriarCarteiraRequest $request, bool $returnJson = false): JsonResponse
    {
        try {
            $model = $this->repository->store($request->all());
            if ($model) {
                return $this->responseApi(
                    $model,
                    true,
                    $this->message('dados.criados.sucesso'),
                    201
                );
            }
            return $this->responseApi(
                $model,
                false,
                $this->message('falha.interna.criar.dados'),
                400
            );
        } catch (ValidationException $e) {
            Log::error($e);
            return $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('falha.interna.criar.dados'),
                $e->status
            );
        } catch (\Exception $e) {
            Log::error($e);
            return $this->responseApi(
                $e->getMessage(),
                false,
                $this->message('falha.interna.criar.dados'),
                $e->getCode()
            );
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/wallets",
     *     summary="Listagem de carteiras",
     *     tags={"wallets"},
     *     description="Lista as carteiras existentes, sendo possível filtrar por redes.",
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="ID endereço da carteira.",
     *         required=false,
     *      @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function listarCarteiras(ListarCarteiraRequest $request)
    {
        try {
            $arrayData = $request->input();
            $arrayData['with_paginate'] = true;
            $carteiras = $this->repository->filter($arrayData, $this->searchWith);

            return $this->responseApi(
                $carteiras,
                true,
                $this->message('dados.retornados.sucesso'),
                200
            );
        } catch (ValidationException $e) {
            Log::error($e);
            return $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('erro.interno.realizar.busca'),
                $e->status
            );
        }
    }

    public function show(string $address): JsonResponse
    {
        try {
            $carteira = $this->repository->findByAddress($address, $this->searchWith)->makeVisible('public_key');
            if (!$carteira) {
                return $this->responseApi(
                    [],
                    false,
                    $this->message('erro.interno.realizar.busca'),
                    400
                );
            }
            return $this->responseApi(
                $carteira,
                true,
                $this->message('dados.retornados.sucesso')
            );
        } catch (ValidationException $e) {
            Log::error($e);
            $response = $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('erro.interno.realizar.busca'),
                $e->status
            );
        }
        return $response;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/wallets/algorithms",
     *     summary="Listagem de algorítmos",
     *     tags={"wallets"},
     *     description="Lista todos os algorítmos disponíveis",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function listarAlgoritimos(Request $request)
    {
        try {
            return $this->responseApi(Algoritmo::get()->makeVisible('algorithm_uid'), true, $this->message('dados.retornados.sucesso'));
        } catch (\Exception $e) {
            Log::error($e);
            return $this->responseApi([], true, $this->message('falha.interna.listar.dados'));
        }
    }
}
