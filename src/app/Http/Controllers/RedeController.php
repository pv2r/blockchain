<?php

namespace App\Http\Controllers;

use App\Models\Rede;
use App\Repositories\RedeRepository;

use Illuminate\Support\Facades\Log;

class RedeController extends BaseController
{
    private $redeRepository;
    public function __construct()
    {
        $this->redeRepository = new RedeRepository(Rede::class);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/networks",
     *     summary="Listagem de redes",
     *     tags={"wallets"},
     *     description="Lista todas as redes disponíveis",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     * @throws ServiceRequestException
     */
    public function __invoke()
    {
        try{
            $resposta = $this->responseApi($this->redeRepository->all(), true, $this->message('dados.retornados.sucesso'), 200);
        } catch (\Exception $e){
            Log::error($e);
            $resposta = $this->responseApi([], false, $this->message('erro.interno.realizar.busca'), 500);
        }
        return $resposta;
    }
}
