<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transacao\AssinarTransacaoRequest;
use App\Http\Requests\Transacao\ProcessarTransacaoRequest;
use App\Http\Requests\TransacaoRequest;
use App\Jobs\SendTransactionJob;
use App\Jobs\SignTransactionJob;
use App\Models\Transacao;
use App\Repositories\TransacaoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class TransacaoController extends BaseController
{
    public function __construct()
    {
        parent::__construct(
            Transacao::class,
            new TransacaoRepository(Transacao::class)
        );
        $this->searchWith = ['transaction_status'];
        $this->showWith = ['transaction_status'];
    }

    /**
     * @OA\Post(
     *     path="/api/v1/transaction/",
     *     summary="Processar uma transação",
     *     tags={"wallets", "transactions"},
     *     description="Criação de uma transação, para ser processar pela blockchain",
     *     @OA\Parameter(
     *         name="network_uid",
     *         in="query",
     *         description="ID da rede",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\RequestBody(
     *        required = true,
     *        description = "Data packet for Test",
     *        @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                property="data",
     *                type="array",
     *                example={{
     *                  "from_address" : "0xb007865Ed0DF59c26bd94F0Bb75cf122F7BDB776",
     *                  "to_address": "0xb007865Ed0DF59c26bd94F0Bb75cf122F7BDB776",
     *                  "nonce" : "0xeD6942f8ED834fc870eD52570",
     *                  "gas_price" : "0x8E42005d4f11200C4be7c43",
     *                  "gas_limit" : "0xeD6942f8ED834fc870eD5",
     *                  "value" : "0x0",
     *                  "data" : "0xeD6942f8ED834fc870eD5257001B976364827b740C4be7c439233236"
     *                }},
     *                @OA\Items(
     *                      @OA\Property(
     *                         property="from_address",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="to_address",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="nonce",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="gas_price",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="gas_limit",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="value",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="data",
     *                         type="string",
     *                         example=""
     *                      ),
     *                ),
     *             ),
     *        ),
     *     ),
     *
     *
     *     @OA\Response(
     *        response="200",
     *        description="Successful response",
     *     ),
     * )
     */
    public function processTransaction(ProcessarTransacaoRequest $request, bool $returnJson = true): JsonResponse
    {
        try {
            $transacao = $this->repository->store($request->all());
            $this->dispatch(new SendTransactionJob($transacao));
            $apiResponse = $this->responseApi(
                $transacao->only('transaction_uid'),
                true,
                "Transaction processing.",
                201
            );
        } catch (ValidationException $e) {
            Log::error($e);
            $apiResponse = $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('falha.interna.criar.dados'),
                $e->status
            );
        } catch (\Exception $e) {
            Log::error($e);
            $apiResponse = $this->responseApi(
                [],
                false,
                $e->getMessage(),
                400
            );
        }
        return $apiResponse;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/transaction/sign",
     *     summary="Assinar uma transação",
     *     tags={"wallets", "transactions"},
     *     description="Assinar uma raw transaction",
     *     @OA\Parameter(
     *         name="network_uid",
     *         in="query",
     *         description="ID da rede",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\RequestBody(
     *        required = true,
     *        description = "Data packet for Test",
     *        @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                property="data",
     *                type="array",
     *                example={{
     *                  "from_address" : "0xb007865Ed0DF59c26bd94F0Bb75cf122F7BDB776",
     *                  "to_address": "0xb007865Ed0DF59c26bd94F0Bb75cf122F7BDB776",
     *                  "value" : "0x0",
     *                  "data" : "0xeD6942f8ED834fc870eD5257001B976364827b740C4be7c439233236"
     *                }},
     *                @OA\Items(
     *                      @OA\Property(
     *                         property="from_address",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="to_address",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="value",
     *                         type="string",
     *                         example=""
     *                      ),
     *                      @OA\Property(
     *                         property="data",
     *                         type="string",
     *                         example=""
     *                      ),
     *                ),
     *             ),
     *        ),
     *     ),
     *
     *
     *     @OA\Response(
     *        response="200",
     *        description="Successful response",
     *     ),
     * )
     */
    public function signTransaction(AssinarTransacaoRequest $request, bool $returnJson = true): JsonResponse
    {
        try {
            $transacao = $this->repository->signTransaction($request->except(['query_string']));
            $this->dispatch(new SignTransactionJob($transacao));

            $apiResponse = $this->responseApi(
                $transacao->only('transaction_uid'),
                true,
                "Transaction processing.",
                201
            );
        } catch (ValidationException $e) {
            Log::error($e);
            $apiResponse = $this->responseApi(
                $this->getValidationErrorMessages($e),
                false,
                $this->message('falha.validacao'),
                $e->status
            );
        } catch (\Exception $e) {
            Log::error($e);
            $apiResponse = $this->responseApi(
                [],
                false,
                $e->getMessage(),
                400
            );
        }

        return $apiResponse;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/transaction/{transaction_uid}/",
     *     summary="Exibição da transação",
     *     tags={"wallets", "transactions"},
     *     description="Detalhes da transação, informando status e algumas inforções básicas",
     *     @OA\Parameter(
     *         name="transaction_uid",
     *         in="query",
     *         description="ID da tranção",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=false
     * )
     */
    public function show(string $uid): JsonResponse
    {
        return parent::show($uid);
    }
}
