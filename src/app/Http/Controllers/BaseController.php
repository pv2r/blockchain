<?php

namespace App\Http\Controllers;

use Exception;
use Throwable;
use App\Messages\Messages;
use Illuminate\Http\Request;
use App\Interfaces\IRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class BaseController extends Controller
{
    const DELETE = 0;
    const CREATE = 1;
    const UPDATE = 2;
    const READ = 3;

    use Messages;

    protected $model;
    protected $rules;
    protected $repository;
    protected int $perPage = 10;
    protected $ruleFieldNames;
    protected $ruleFieldValueNames;
    protected $ruleCustomMessages;

    /**
     * Relacionamentos para o método index.
     * @var array
     */
    protected $indexWith = [];

    /**
     * Relacionamentos para o método show.
     * @var array
     */
    protected $showWith = [];

    /**
     * Relacionamentos para o método search.
     * @var array
     */
    protected $searchWith = [];

    public function __construct(?string $model = null, ?IRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;

        $this->repository = $repository;
        // pequena gambiarra emocional
        if (!$repository) {
            $this->repository = new BaseRepository($this->model);
        }
    }

    /**
     * Retorno padrao para as resposta para API
     * @param $data
     * @param bool $status
     * @param string $message
     * @param int $status_code
     * @return JsonResponse
     */
    public function responseApi($data, bool $status = true, string $message = '', int $status_code = 200): JsonResponse
    {
        // TODO: deixando comentado até sanarmos nossas dúvidas de como proceder com essa criptografia

        //        if(config('app.env') != 'local'){
        //            $data = criptografar($data);
        //        }

        return response()->json([
            'success' => $status,
            'message' => $message,
            'data' => $data
        ], $status_code);
    }
    //
    //    /**
    //     * @return JsonResponse
    //     */
    //    public function index(): JsonResponse
    //    {
    //        try {
    //            $data = $this->model::paginate($this->perPage);
    //
    //            if (!empty($this->indexWith)) {
    //                $data->load($this->indexWith);
    //            }
    //
    //            return $this->responseApi($data, true, $this->message('dados.retornados.sucesso'));
    //        } catch (Throwable $e) {
    //            Log::error($e->getMessage());
    //
    //
    //            return $this->responseApi([], false, $this->message('falha.interna.listar.dados'), 500);
    //        }
    //    }

    /**
     * Usar este endpoint quando quiser exibir dados sem paginação, para listar em um select por exemplo.
     * @return JsonResponse
     */
    public function getAll(bool $returnJson = true)
    {
        try {

            if (!$returnJson) {
                return $this->model::all();
            }
            return $this->responseApi($this->model::all(), true, $this->message('dados.retornados.sucesso'));
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return $this->responseApi([], false, $this->message('falha.interna.listar.dados'), 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request, bool $returnJson = true)
    {
        try {
            $this->makeValidation($request->all(), self::CREATE);

            $data = $this->repository->store($request->all());
            if (!$returnJson) {
                return $data;
            }
            return $this->responseApi($data, true, $this->message('dados.criados.sucesso'), 201);
        } catch (ValidationException $e) {
            return $this->responseApi($e->errors(), false, $this->message('falha.interna.criar.dados'), 500);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return $this->responseApi($e, false, $this->message('falha.interna.criar.dados'), 500);
        }
    }

    /**
     * @param string $uid
     * @return JsonResponse
     */
    public function show(string $uid): JsonResponse
    {
        try {
            $data = $this->model::find($uid);
            if (is_null($data)) {
                return $this->responseApi([], false, $this->message('registro.nao.existe'), 404);
            }

            if (!empty($this->showWith)) {
                $data->load($this->showWith);
            }

            return $this->responseApi($data, true, $this->message('dados.retornados.sucesso'));
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return $this->responseApi([], false, $this->message('falha.interna.exibir.dados'), 500);
        }
    }

    /**
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     */
    public function update(Request $request, string $uid, bool $returnJson = true)
    {
        try {
            $this->makeValidation($request->all(), self::UPDATE);
            $data = $this->repository->update($uid, $request->all());

            if (is_null($data)) {
                return $this->responseApi([], false, $this->message('registro.nao.existe'), 404);
            }
            if (!$returnJson) {
                return $data;
            }
            return $this->responseApi($data, true, $this->message('dados.atualizados.sucesso'));
        } catch (ValidationException $e) {
            return $this->responseApi($e->errors(), false, $this->message('falha.interna.atualizar.dados'), 500);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return $this->responseApi([], false, $this->message('falha.interna.atualizar.dados'), 500);
        }
    }

    /**
     * @param string $uid
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(string $uid, Request $request): JsonResponse
    {
        try {
            $this->makeValidation($request->all(), self::DELETE);
            $data = $this->repository->destroy($uid, $request->deleted_by);

            if ($data == 0) {
                return $this->responseApi([], false, $this->message('registro.nao.existe'), 404);
            }

            return $this->responseApi([], true, $this->message('dados.excluidos.sucesso'));
        } catch (ValidationException $e) {
            return $this->responseApi($e->errors(), false, $this->message('falha.interna.excluir.dados'), 500);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return $this->responseApi([], false, $this->message('falha.interna.excluir.dados'), 500);
        }
    }

    /**
     * Busca de dados utilizando os campos do model e data de criacao
     * @param Request $request
     * @param array $with
     * @return JsonResponse
     */
    public function search(Request $request, bool $getFirst = false, $with = [], bool $returnJson = true)
    {
        $search = $request->except(['page', 'forExport']);

        $model = new $this->model;

        $forExport = ($request->has('forExport')) ? true : false;

        //incluindo campos personalizaveis na busca
        $fieldsSearch = array_merge(['created_at', $model->getKeyName()], $model->getFillable());
        Log::info('fields search', [$fieldsSearch]);
        //validando campos permitidos
        foreach ($search as $key => $value) {
            if (!array_keys($fieldsSearch, $key)) {
                unset($search[$key]);
            }
        }

        //retornando busca vazia
        if (empty($search)) {

            $resultIfEmptySearch = $model::orderBy('created_at', 'desc');

            if (!empty($this->searchWith)) {
                foreach ($this->searchWith as $item) {
                    $resultIfEmptySearch->with($item);
                }
            }

            // Caso seja para exportar os dados da pesquisa para um CSV
            if ($forExport) {
                return $this->responseApi($resultIfEmptySearch->get());
            }

            if ((bool)$request->get('with_paginate')) {
                return $this->responseApi($resultIfEmptySearch->paginate($this->perPage));
            }

            return $this->responseApi($resultIfEmptySearch->get());
        }

        $result = $this->repository->filter($search, $this->searchWith);

        // Caso seja para exportar os dados da pesquisa para um CSV
        if ($forExport) {
            return $this->responseApi($result);
        }

        if ((bool) $request->get('with_paginate')) {
            return $this->responseApi($result->paginate($this->perPage));
        }

        if (!empty($this->searchWith)) {
            foreach ($this->searchWith as $item) {
                $result->with($item);
            }
        }

        $result = $getFirst ? $result->first() : $result;
        if (!$returnJson) {
            return $result;
        }
        return $this->responseApi($result);
    }

    /**
     * Busca avancada
     * Estrutura padrao:
     * {"select":"*","where":[{"table":"TABELA", "column":"COLUNA", "operation":"=", "link":"AND"}]
     * "join":[{"type":"JOIN", "table":"TABELA", "from":"TABELA_JOIN", "operation":"=","to":"TABELA_JOIN_2"}]
     * "orderBy":{"type":"DESC","column":"COLUNA"}
     * @param Request $request
     * @return JsonResponse
     */
    public function searchAdvanced(Request $request, bool $returnJson = true)
    {
        try {
            $forExport = ($request->has('forExport')) ? true : false;

            $query = $this->repository->advancedFilter($request);

            if ($forExport) {
                return $this->responseApi($query->get());
            }
            if (!$returnJson) {
                return $query;
            }
            return $this->responseApi($query->paginate($this->perPage));
        } catch (Exception $e) {
            if (config('app.env') == 'local') {
                return $this->responseApi([], false, $e->getMessage(), 500);
            }
            return $this->responseApi([], false, $e);
        }
    }

    /**
     * Count de ocorrencias na tabela
     * @param string $key
     * @param string $value
     * @return JsonResponse
     */
    public function countBy(string $key, string $value, bool $returnJson = false)
    {
        try {
            $data = $this->model::where($key, $value)->count();
            if (!$returnJson) {
                return $data;
            }
            return $this->responseApi($data);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return $this->responseApi([], false, $this->message('erro.interno.realizar.busca'));
        }
    }
}
