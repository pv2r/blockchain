<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', \App\Http\Controllers\HealthCheckController::class);
Route::group(['prefix' => 'v1', 'middleware' => ['forceApi','client']], function () {
    Route::get('/', function () {
        return redirect('/api/documentation');
    });
    Route::group(['prefix' => 'wallets'], function () {
        Route::post('/', [\App\Http\Controllers\CarteiraController::class, 'criarCarteira']);
        Route::get('/', [\App\Http\Controllers\CarteiraController::class, 'listarCarteiras'])->name("api.v1.wallets.index");
        Route::get('/algorithms', [\App\Http\Controllers\CarteiraController::class, 'listarAlgoritimos']);
        Route::get('/public_key/address/{address_uid}', \App\Http\Controllers\ChavePublicaController::class);
        Route::get('/{address}', [\App\Http\Controllers\CarteiraController::class, 'show']);
    });
    Route::group(['prefix' => 'transaction'], function () {
        Route::post('/', [\App\Http\Controllers\TransacaoController::class, 'processTransaction']);
        Route::post('sign', [\App\Http\Controllers\TransacaoController::class, 'signTransaction']);
        Route::get('{uid}', [\App\Http\Controllers\TransacaoController::class, 'show']);
    });
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [\App\Http\Controllers\CategoriaController::class,'getAll']);
    });
    Route::group(['prefix' => 'frequencies'], function () {
        Route::get('/', [\App\Http\Controllers\FrequenciaController::class,'getAll']);
    });
    Route::group(['prefix' => 'profiles'], function () {
        Route::get('/', [\App\Http\Controllers\PerfilController::class,'getAll']);
    });
    Route::group(['prefix' => 'networks'], function () {
        Route::get('/', \App\Http\Controllers\RedeController::class);
    });
});
