<?php

namespace Tests\Feature;

use App\Models\Algoritmo;
use App\Models\Carteira;
use App\Models\Categoria;
use App\Models\Perfil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CarteiraControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_deve_listar_todas_as_carteiras()
    {
        $totalCarteiras = 2;
        Carteira::factory()
            ->has(Algoritmo::factory())
            ->has(Perfil::factory())
            ->has(Categoria::factory())
            ->count($totalCarteiras)
            ->create();
        $response = $this->get('api/v1/wallets');
        $response->assertStatus(200);
        $this->assertEquals($totalCarteiras, $response->json()['data']['total']);
    }

    public function test_parametro_frequency_uid_deve_ser_valido_ao_fazer_um_filtro_das_carteiras()
    {
        $mockParams = [
            "frequency_uid" => "any_frequency_uid",
        ];
        $route = route('api.v1.wallets.index', $mockParams);
        $response = $this->get($route);
        $response
            ->assertStatus(422)
            ->assertJson([
                "data" => [
                    "frequency_uid" => ["The selected frequency uid is invalid."]
                ]
            ]);
    }

    public function test_parametro_algorithm_uid_deve_ser_valido_ao_fazer_um_filtro_das_carteiras()
    {
        $mockParams = [
            "algorithm_uid" => "any_algorithm_uid",
        ];
        $route = route('api.v1.wallets.index', $mockParams);
        $response = $this->get($route);
        $response
            ->assertStatus(422)
            ->assertJson([
                "data" => [
                    "algorithm_uid" => ["The selected algorithm uid is invalid."]
                ]
            ]);
    }

    public function test_parametro_profile_uid_deve_ser_valido_ao_fazer_um_filtro_das_carteiras()
    {
        $mockParams = [
            "profile_uid" => "any_profile_uid",
        ];
        $route = route('api.v1.wallets.index', $mockParams);
        $response = $this->get($route);
        $response
            ->assertStatus(422)
            ->assertJson([
                "data" => [
                    "profile_uid" => ["The selected profile uid is invalid."]
                ]
            ]);
    }

    public function test_parametro_category_uid_deve_ser_valido_ao_fazer_um_filtro_das_carteiras()
    {
        $mockParams = [
            "category_uid" => "any_category_uid",
        ];
        $route = route('api.v1.wallets.index', $mockParams);
        $response = $this->get($route);
        $response
            ->assertStatus(422)
            ->assertJson([
                "data" => [
                    "category_uid" => ["The selected category uid is invalid."]
                ]
            ]);
    }
}
