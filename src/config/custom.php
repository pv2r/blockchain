<?php

use Illuminate\Support\Facades\Config;

return [
    //SERVICE
    'SERVICE_URL_BLOCKCHAIN' => env('SERVICE_URL_BLOCKCHAIN'),
    'CRIPTOGRAFIA' => "AES-128-CBC",
    'PROJETO' => env('APP_NAME')
];
