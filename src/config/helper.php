<?php
if (!function_exists('traduzir_input')) {
    function traduzir_input(array $requestInputs): array
    {
        foreach ($requestInputs as $key => $value) {
            $stringUppercase = ucfirst($requestInputs[$key]);
            $requestInputs[$key] = strtolower(__($stringUppercase));
        }
        return $requestInputs;
    }
}
if (!function_exists('is_local')) {
    function is_local()
    {
        if(env('APP_ENV') === "local" || env('APP_ENV')=== "testing"){
            return true;
        }
        return false;
    }
}
if (!function_exists('criptografar')) {
    /**
     *
     */
    function criptografar(array $data, $cipher = null, $key = null, $iv = null)
    {
        $cipher = config('custom.CRIPTOGRAFIA');
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $key = openssl_random_pseudo_bytes($ivlen);
        // TODO: salvar as chaves para o usuário
        $encryptedData = openssl_encrypt(json_encode($data), $cipher, $key, $options = 0, $iv);
        return $encryptedData;
    }
}