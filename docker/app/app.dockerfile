FROM php:8.1-fpm-alpine AS base

RUN apk add --no-cache shadow openssl bash mysql-client git \
    && docker-php-ext-install pdo pdo_mysql

RUN echo "PS1='\s-\v \$PWD \$ '" >> /home/www-data/.bashrc \
    && usermod -u 1000 www-data

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www
COPY --chown=1000:www-data ./docker/app/entrypoint.sh /docker/
COPY --chown=1000:www-data ./src/ .
RUN composer install \
    && chown -R 1000:www-data vendor
# Install supervisor
RUN apk --no-cache add supervisor
# Added supervisor config
COPY ./docker/app/supervisor.conf /etc/supervisord.conf
RUN chown 1000:www-data .


FROM base AS test
RUN php artisan passport:keys --force && php artisan test


FROM base AS prod
USER www-data
VOLUME /var/www
ARG ENV
RUN rm -rf html && ln -s public html \
    && cp -vfa .env.$ENV .env \
    && rm -rf .env.*
EXPOSE 9000
ENTRYPOINT [ "/docker/entrypoint.sh" ]