#!/bin/bash

if [[ "$ENV" != "local" ]]; then
    echo "$OAUTH_PRIVATE_KEY" > storage/oauth-private.key
    echo "$OAUTH_PUBLIC_KEY" > storage/oauth-public.key
else
  cp .env.local .env
fi

php artisan test