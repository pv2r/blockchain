#!/bin/bash

if [[ "$ENV" != "local" ]]; then
    echo "$OAUTH_PRIVATE_KEY" > storage/oauth-private.key
    echo "$OAUTH_PUBLIC_KEY" > storage/oauth-public.key
else
  cp .env.local .env
fi

php artisan key:generate
php artisan migrate
php artisan l5-swagger:generate

/usr/bin/supervisord -n -c /etc/supervisord.conf